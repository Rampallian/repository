#include <iostream>
#include <cstdlib>
#include <vector>
using namespace std;

void storeRandomNumbers(vector<int>& v, int low, int high);
void output(const vector<int>& v);
void bubbleSort(vector<int>& v);

int main() 
{
    srand(time(0));
    
    // "Bubble" two numbers at a time
    // Compare and swap if the second is smaller than the first
    // Iterate through N times through the container
    vector<int> v(10);
    
    storeRandomNumbers(v, 1, 50);
    
    output(v);
    
    bubbleSort(v);
    
    output(v);
    
    return 0;
}

void storeRandomNumbers(vector<int>& v, int low, int high)
{
    for(int i = 0; i < v.size(); i++)
    {
        v[i] = rand() % (high - low + 1) +  low;
    }
}

void output(const vector<int>& v)
{
    for(int i = 0; i < v.size(); i++)
    {
        cout << v[i] << " ";
    }
    cout << endl;
}

/**
 * The function executes bubble sort on a vector
 * Sorts the vector in ascending order
 * @param v
 */
void bubbleSort(vector<int>& v)
{
    
    // Iterate N number of times
    for(int i = 0; i < v.size(); i++)
    {
        cout << "Iteration " << i << endl;
        // Bubble two values at a time, and swap if needed
        for(int j = 0; j < v.size() - 1; j++)
        {
            if (v[j] > v[j + 1]) // Verify bounds
            {
                swap(v[j], v[j + 1]);
            }
        }
        output(v);
    }
}