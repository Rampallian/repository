#include <vector>
#include <cstdlib>
#include <iostream>
using namespace std;

void storeRandomNumbers(vector<int>& v, int low, int high);
void output(const vector<int>& v);

void insertionSort(vector<int>& v);

int main() 
{
    srand(time(0));
    
    vector<int> v(10);
    
    storeRandomNumbers(v, 1, 20);
    
    output(v);

    insertionSort(v);
    
    
    
    return 0;
}

void storeRandomNumbers(vector<int>& v, int low, int high)
{
    for(int i = 0; i < v.size(); i++)
    {
        v[i] = rand() % (high - low + 1) +  low;
    }
}

void output(const vector<int>& v)
{
    for(int i = 0; i < v.size(); i++)
    {
        cout << v[i] << " ";
    }
    cout << endl;
}

/**
 * Sorts a vector using insertion sort
 * Sorts in ascending order
 * @param v
 */
void insertionSort(vector<int>& v)
{
    // Iterate N -1 times
    for(int i = 1; i < v.size(); i++)
    {
        cout << "Iteration: " << i << endl;
        int cur = v[i];
        
        int j = i;
        
        // j > 0 handles the case of inserting at the beginning
        // cur < v[j - 1] checks to see if the values previosuly is larger
        while(j > 0 && cur < v[j - 1])
        {
            // We need to shift the value over because its bigger than cur
            v[j] = v[j - 1];
            j--;
        }
        
        v[j] = cur;
        
        output(v);
    }
}

