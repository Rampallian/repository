#include <iostream>
#include <cstdlib>
#include <vector>
using namespace std;

void coutt(int v, int v1);
void output(int [], int);
void output(const vector<int>& v);
void storeRandomValues(int [], int);
void storeRandomNumbers(vector<int>& v, int low, int high);
int getDASize();
int* createDynamicArray(int size);
int binarySearch(const vector<int>& v, int val);
void selectionSort(vector<int>& v);

int main() 
{
    // Seed!
    srand(time(0));
    
    // main menu
    cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n" << endl;
    cout << "Homework 10" << endl;
    cout << "Which problem would you like to run?" << endl;
    cout << "------------------------------------" << endl;
    cout << "1) Problem 1" << endl;
    cout << "2) Problem 2" << endl;
    cout << "3) Problem 3" << endl;
    cout << "4) Problem 4" << endl;
    cout << "5) Problem 5" << endl;
    cout << "6) Problem 6" << endl;
    cout << "------------------------------------" << endl;
    cout << "Enter in the problem number: ";
    int choice;
    cin >> choice;
    cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n" << endl;
    
    switch(choice)
    {
        case 1:
        {
            int v = 42;
            int v1 = v;

            // initializing pointers set to the address of v
            int* p = &v; 
            int* p1 = &v;

            cout << "Initial values:" << endl;
            coutt(v, v1);

            // change value using variable
            v = 12;

            cout << "First change:" << endl;
            coutt(v, v1);

            // change value using pointer
            *p = 25;

            cout << "Second change:" << endl;
            coutt(v, v1);

            // change value using other pointer
            *p1 = 60;

            cout << "Third change:" << endl;
            coutt(v, v1);
        }
        break;
        case 2:
        {
            cout << "Provide a size for the dynamic array: ";
            int size;
            cin >> size;
            
            int* array = new int[size];
            
            storeRandomValues(array, size);
            output(array, size);
        }
        break;
        case 3:
        {
            int size = getDASize();
            int* da = createDynamicArray(size);
            storeRandomValues(da, size);
            output(da, size);
        }
        break;
        case 4:
        {
            int size = 5;
            int array[size];
            int* pointer = array;
            
            cout << pointer << endl;
            
        }
        break;
        case 5:
        {
            int size = 5;
            int* da = createDynamicArray(size);
            
        }
        break;
        case 6:
        {
            vector<int> v(10000);
            
            storeRandomValues(v);
            
            //selectionSort(v);
            
            cout << "Enter a value to search: ";
            int search;
            cin >> search;
            
            int loc = binarySearch(v, search);
            
            output(v);
            
            if (loc == -1)
            {
                cout << "That number was not found!!!" << endl;
            }
            else
            {
                cout << "The number is located at index: " << loc << endl;
            }
        }
        break;
        default:
            cout << "Invalid Input! Exiting..." << endl;
    }
    return 0;
}

/**
 * Simply outputs two variables
 * @param v First value
 * @param v1 Second value
 */
void coutt(int v, int v1)
{
    cout << "v: " << v << endl;
    cout << "v1: " << v1 << endl << endl;;
}

void output(int a[], int size)
{
    for(int i = 0; i < size; i++)
    {
        cout << a[i] << " ";
    }
    cout << endl;
}
void storeRandomValues(int a[], int size)
{
    for(int i = 0; i < size; i++)
    {
        a[i] = rand() % 15 + 1;
    }
}

int getDASize()
{
    cout << "Provide a size for the dynamic array: ";
    int size;
    cin >> size;
    
    return size;
}

int* createDynamicArray(int size)
{
    int* a = new int [size];
    
    return a;
}

/** Copied from Binary Search module
 * Performs binary search on a vector
 * Returns -1 if not found
 * The vector must be sorted
 * @param 
 * @param val
 * @return 
 */
int binarySearch(const vector<int>& v, int val)
{
    // Check for empty container
    if (v.size() == 0)
    {
        return -1;
    }
    
    int low = 0;
    int high = v.size() - 1;
    int middle = (low + high) / 2;
    int guess = v[middle];
    
    // Keep splitting the search space in half
    // Search only what you need to search
    while(guess != val && low <= high)
    {
        // Get the new middle
        if (guess < val)
        {
            // Go search to the right of it
            low = middle + 1;
            middle = (low + high) / 2;
            guess = v[middle];
        }
        else
        {
            // Move the high value down
            high = middle - 1;
            middle = (low + high) / 2;
            guess = v[middle];
        }
    }
    if (low > high)
    {
        return -1;
    }
    return middle;
}

void output(const vector<int>& v)
{
    for(int i = 0; i < v.size(); i++)
    {
        cout << v[i] << " ";
    }
    cout << endl;
}

/** Copied from Selection Sort Module
 * Sorts the vector using selection sort, in ascending order
 * @param v
 */
void selectionSort(vector<int>& v)
{
    // Number of iterations ( last one is already sorted )
    for(int i = 0; i < v.size() - 1; i++)
    {
        // Find min
        // Minimum is going to be the minimum location
        int min = i;
        
        // Selecting the smallest value in the unsorted region
        for(int j = i; j < v.size(); j++)
        {
            if (v[j] < v[min])
            {
                min = j;
            }
        }
        swap(v[i], v[min]);
    }
}

void storeRandomNumbers(vector<int>& v, int low, int high)
{
    for(int i = 0; i < v.size(); i++)
    {
        v[i] = rand() % (high - low + 1) +  low;
    }
}