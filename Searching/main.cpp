#include <iostream>
#include <cstdlib>
#include <vector>
using namespace std;

// Linear - returns location
int linearSearch(const vector<int>&, int val);

int binarySearch(const vector<int>&, int val);
void storeRandomNumbers(vector<int>& v, int low, int high);
void output(const vector<int>& v);
void selectionSort(vector<int>& v);


int main() 
{
    srand(time(0));
    int size = 20;
    vector<int> v(size);
    
    storeRandomNumbers(v, 1, 20);
    output(v);
    
    cout << "Provide a number to find: ";
    int num;
    cin >> num;
    
    int location = linearSearch(v, num);
    
    if (location == -1)
    {
        cout << "That number was not found!" << endl;
    }
    else
    {
        cout << "The number is located at: " << location << endl;
    }
    
    cout << "Using binary search" << endl;
    
    // Make sure the vector is sorted
    selectionSort(v);
    output(v);
    
    cout << "Enter another number to find: ";
    cin >> num;
    
    location = binarySearch(v, num);
    
    if (location == -1)
    {
        cout << "That number was not found!" << endl;
    }
    else
    {
        cout << "The number is located at: " << location << endl;
    }
    
    return 0;
}
/**
 * This function returns the location of a given value. 
 * If the the value si not found then return -1.
 * @param 
 * @param val
 * @return 
 */
int linearSearch(const vector<int>& v, int val)
{
    // For loop to search for the number
    for(int i = 0; i < v.size(); i++)
    {
        if (v[i] == val)
        {
            return i;
        }
    }
    // If i reach here then I never found the code
    return -1;
}

/**
 * Performs binary search on a vector
 * Returns -1 if not found
 * The vector must be sorted
 * @param 
 * @param val
 * @return 
 */
int binarySearch(const vector<int>& v, int val)
{
    // Check for empty container
    if (v.size() == 0)
    {
        return -1;
    }
    
    int low = 0;
    int high = v.size() - 1;
    int middle = (low + high) / 2;
    int guess = v[middle];
    
    // Keep splitting the search space in half
    // Search only what you need to search
    while(guess != val && low <= high)
    {
        cout << "Low: " << low << endl;
        cout << "High: " << high << endl;
        cout << "Middle: " << middle << endl << endl;
        // Get the new middle
        if (guess < val) // Guess too low
        {
            cout << "Too low!" << endl;
            // Go search to the right of it
            low = middle + 1;
            middle = (low + high) / 2;
            guess = v[middle];
        }
        else // Guess too low
        {
            cout << "Too high!" << endl;
            // Move the high value down
            high = middle - 1;
            middle = (low + high) / 2;
            guess = v[middle];
        }
    }
    if (low > high)
    {
        // Did not find
        return -1;
    }
        
    
    return middle;
}

void storeRandomNumbers(vector<int>& v, int low, int high)
{
    for(int i = 0; i < v.size(); i++)
    {
        v[i] = rand() % (high - low + 1) +  low;
    }
}

void output(const vector<int>& v)
{
    for(int i = 0; i < v.size(); i++)
    {
        cout << v[i] << " ";
    }
    cout << endl;
}

void selectionSort(vector<int>& v)
{
    // Number of iterations ( last one is already sorted )
    for(int i = 0; i < v.size() - 1; i++)
    {
        
        
        // Find min
        // Minimum is going to be the minimum location
        int min = i;
        
        // Selecting the smallest value in the unsorted region
        for(int j = i; j < v.size(); j++)
        {
            if (v[j] < v[min])
            {
                min = j;
            }
        }
        // Swap
        swap(v[i], v[min]);
        
        
    }
}


