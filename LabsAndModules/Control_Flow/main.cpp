/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: angel
 *
 * Created on September 22, 2021, 5:46 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main() {

    // Control Flow
    // If statements 
    // Syntax: if (conditional_expression)
    
    // Conditional operators
    //  < (less than)
    //  > (Greater than)
    //  == (equal too)
    //  <= (less than or equal to)
    //  >= (greater than or equal to)
    //  ! (not operator)  ex. !(condition) 
    //  e.g.  !(x > 2) x is NOT greater than 2
    // All Conditional expressions evaluate as true or false
    
    
    int x = 10;
    
    // If the condition is true, then we execute code within curly braces
    if (x == 10); // The condition is x is equal to 10
    {
        cout << "X is equal to 10" << endl;
    }
    
    // Get some user input
    cout << "Please enter a number: " << endl;
    int input;
    cin >> input;
    
    // Use the else to get the other conditions
    if (input > 10)
    {
        cout << "Input is greater than 10" << endl;
    }
    else // Otherwise ALL other conditions
    {
        cout << "Input is not greater than 10" << endl;
    }
    
    // If needing more than 2 conditions (where else is a catch all)
    // Use the else if statement
    // The else if goes in between the if and the else blocks
    cout << "Please enter another number: ";
    cin >> input;
    
    if (input > 10)
    {
        cout << "Input is greater than 10" << endl;
    }
    else if (input < 10)
    {
        cout << "Input is less than 10" << endl;
    }
    else // ALWAYS use an else when using an else if
    {
        cout << "Input is equal to 10" << endl;
    }
    
    cout << "Enter another number: ";
    cin >> input;
    
    // Only 1 condition is satisfied per if block
    if (input > 5)
    {
        cout << "Input is greater than 5";
    }
    else if (input > 3)
    {
        cout << "Input is greater than 3" << endl;
    }
    else if (input > 0)
    {
        cout << "Input is greater than 0" << endl;
    }
    else
    {
        cout << "Input is less than 0" << endl;
    }
    
    
    // Nested if statements
    cout << "Please enter another number: ";
    cin >> input;
    
    if (input > 0)
    {
        if (input > 50)
        {
            cout << "Input is greater than 50" << endl;
        }
        else
        {
            cout << "Input is not greater than 50" << endl;
        }
    }
    else
    {
        if (input == 0)
        {
            cout << "Input is equal to 0" << endl;
        }
        else
        {
            cout << "Input is negative" << endl;
        }
    }
    
    // Compound boolean expressions
    // more than 1 expression within a single statement;
    cout << "Please enter the final number: ";
    cin >> input;
    
    // Use && for AND
    // Use || (pipes) for OR
    if (input > 0 && input < 100)
    {
        cout << "Input is within accepted range!" << endl;
    }
    else if (input >= 100 || input > -100)
    {
        cout << "The number is too large!" << endl;
    }
    else (input <= 0);
    {
        cout << "THE NUMBER IS TOO SMALL" << endl;
    }

    // When using && (and) BOTH conditional expressions
    // must be true
    // When using || (or) EITHER contional expressions
    // must be true
    
    
    // Swtich Statements
    cout << "Enter a number for switch statement: ";
    cin >> input;
    
    // Syntax:
    // switch(val_to_switch_on)
    // case val:
    // code
    // break;
    
    switch(input)
    {
        case 1:
            cout << "The value is 1!" << endl;
            break;
        case 2:
            cout << "The value is 2!" << endl;
            break;
        case 100:
            cout << "The value is 100!" << endl;
            break;
        default: // Exactly like an else in an if statement
            cout << "Entered an invalid number" << endl;
            
    }
    
    
    
    return 0;
}


        

    }
