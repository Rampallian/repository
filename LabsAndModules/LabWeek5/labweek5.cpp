/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: angel
 *
 * Created on September 28, 2021, 3:46 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main() {

    
    
    // Prompt the user
    cout << "1) Loops with strings" << endl;
    cout << "2) Babylonian Algorithm" << endl;
    
    cout << "Please enter an option: " << endl;
    int option;
    cin >> option;
    
    // Declare the string outside the switch
    string name;
    
    switch(option)
    {
        case 1:
            cout << "Loops!" << endl;
            
            name = "Angel";
            
            /*  A A A A A
             *  N N N N N
             *  G G G G G
             *  E E E E E
             *  L L L L L
             */ 
            cout << name.size() << endl;
            
            for (int i = 0; i < name.size(); i++)
            {
                cout << i << ": ";
                for (int j = 0; j < name.size(); j++)
                {
                    // name[i] = 'x'
                    // Use the subscript operator []
                    // to access individual characters
                    cout << name[i] << " ";
                }
                cout << endl;
            }
            break;
        case 2:
            cout << "Babylonian!" << endl;
            
            cout << "What value do you want to square root: ";
            double input;
            cin >> input;
            
            double guess, r, newGuess;
            
            guess = input / 2;  // Initial guess (part a)
            
            r = input / guess;  // Part b
            
            newGuess = (guess + r) / 2;  // Part c
            
            cout << "Guess: " << guess << endl;
            cout << "New Guess: " << newGuess << endl;
            
            // Algorithm gets within -+ 1%
            // 101 / 100 = 1.01
            // 100 / 101 = .99
            while (guess / newGuess < .9999 || guess / newGuess > 1.0001)
            {
                guess = newGuess; // Update the old guess to the current guess
                r = input / guess;
                newGuess = (guess + r) / 2;
                cout << "Guess: " << guess << endl;
                cout << "New Guess: " << newGuess << endl;
            
            }
            
                    
            
            break;
        default:
            cout << "No valid input... exiting... " << endl;
    }
    
    
    
    
    
    
    
    
    // STRINGS
    
    // String data type
    string s = "Hello World"; // Hello world is a string literal
    
    // Primitive data types
    int i;
    double d;
    bool b;
    char c;
    
    // String is known as a class
    // s is a variable that holds a string object
    // An object is an instance of a class
    // A class is a user-defined data type
    
    string s2 = "Dave";
    
    cout << i << " " << s << endl;
    
    // Objects contain properties
    // String is a set of characters
    
    // Objects contain member functions
    // A function is a specific task e.g. srand(), getline(cin, s)
    // A member is the object which the function acts upon
    // A member function only acts on the member
    // A member function is used with the dot operator (.)
    
    // String member functions
    // Length of a string
    // Use .length() or .size()
    // Returns the number of characters in a string
    int length = s2.length();
    cout << "The length of s2 is: " << length << endl;
    
    cout << "The length of s is: " << s.length() << endl;
    
    cout << "The size of s is: " << s.size() << endl;
    
    
    // Not a function!
    // int x = (2 + 3) / 5;
    
    // 2nd member function
    // .substr(int location)
    // .substr(int location, int count) // Count is # of characters
    //e.x. s.substr(0, 1);
    // Returns the substring of a string
    
    // "HELLO WORLD"
    // Start location at 0 <-- C++ is a zero based index langauge
    // H -> Location 0
    // E -> Location 1
    // L -> Location 2
    // L -> Location 3
    // O -> Location 4
    
    // e.x. Get the sub-string "LLO"
    cout << s.substr(2, 3) << endl;
    
    // e.x. Get the sub-string "ORL"
    cout << s.substr(7, 3) << endl;
    
    // Get " World"
    cout << s.substr(5) << endl;
    
    // Get "D"
    cout << s.substr(s.size() - 1) << endl;
    
    // Find member function
    // Find any given sub-string from a string
    // Returns the location (index) of that string
    // .find(string)
    cout << s.find("O") << endl;
    
    // e.x. Find the O
    int location = s.find("O");
    cout << s.find("O") << endl; //Outputs -1. Means no string was found
    
    // Find the 0
    location = s.find("o");
    cout << "Location of o: " << location << endl;
    
    // Find the "orl"
    cout << "Location of orl: " << s.find("orl") << endl;
    
    
    
    
    
    
    // Pseudocode
    //----------------------------------------------------
    // Not actual code
    // A condensed more English version of code
    // Pseudo code the craps game
    
    // plays: get the number of times the user wants to play
    // validate that the number of times played is less than times wanting to play
    // (if times played is less than total plays)
    
    
    // (if timesplayed < totalplays)
    //   roll 2 dice and store value
    //   if value 1 is equal to 11 or 7
    //     record win
    //   if value 1 is equal to 2, 3, or 12
    //     record loss
    //   else go to round 2
    //     while value2 is not equal to 7 nor value 1 rool again
    //       value2: 2 new dice roll
    //       if value2 is equal to value1
    //         record win
    //       if value2 is 7
    //         record loss
    //       else roll again
    //   increase timesplayed by 1
    
    
   
    
    
    
    // String objects
    string name2 = "Dave Matthews";
    
    // Location and Index
    // name has 13 characters
    cout << "Num characters: " << name2.length() << endl;
    
    // The largest index is 12
    // This is because the index starts at 0
    
    // Use the subscript operator []
    // An index is inside the square brackets [2]
    // example: name[0] 
    // Return the character at that location
    cout << "Character at location 0: " << name2[0] << endl;
    
    cout << "Character at location 2: " << name2[1] << endl;
    
    // Get character at location 20
    // Logic error -> bounds error
    cout << "Location 20: " << name[20] << endl;
    
    
    
    
    return 0;
}

