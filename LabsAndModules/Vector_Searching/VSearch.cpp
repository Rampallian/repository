#include <vector>
#include <cstdlib>
#include <iostream>
using namespace std;

int maxValue(const vector<int>& v);

int minValue(const vector<int>& v);

double averageValue(const vector<int>& v);

void output(const vector<int>& v);

int main() 
{
    vector<int> values;
    
    values.push_back(3);
    values.push_back(1);
    values.push_back(10);
    values.push_back(25);
    
    output(values);
    cout << "Max value: " << maxValue(values) << endl;
    
    cout << "Min value: " << minValue(values) << endl;
    
    cout << "Average value: " << averageValue(values) << endl;
    
    return 0;
}

/**
 * The functions figures out the largest number within the vector
 * There is no current error checking on the vector
 * @param v A vector
 * @return Returns the largest number
 */
int maxValue(const vector<int>& v)
{
    // Create a temporary max value
    int max = -99999999; // Very tiny value
    
    // Check every single element
    // If the value is larger, it is the new max
    for(int i = 0; i < v.size(); i++)
    {
        // Compare value to the current max
        if (v[i] > max)
        {
            // We have a new maximum
            max = v[i];
        }
    }
    
    return max;
}

/**
 * This returns the smallest value within a vector
 * @param v
 * @return 
 */
int minValue(const vector<int>& v)
{
    // Temp variable for the largest value
    int min = 99999999;
    
    // Loop across entire vector
    for(int i = 0; i < v.size(); i++)
    {
        // Compare element to current minimum
        if (v[i] < min)
        {
            // New minimum
            min = v[i];
        }
    }
    
    return min;
}

/**
 * Return the average value of the vector
 * @param v
 * @return 
 */
double averageValue(const vector<int>& v)
{
    // Need to keep track of the total value
    int total = 0;
    
    // Loop across all elements and add to total
    for (int i = 0; i < v.size(); i++)
    {
        total += v[i];
    }
        
    // Don't forget to static cast
    return total / static_cast<double>(v.size());
}

void output(const vector<int>& v)
{
    for (int i = 0; i < v.size(); i++)
    {
        cout << v[i] << " ";
    }
    cout << endl;
}
    

