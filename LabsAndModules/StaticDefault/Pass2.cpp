

#include <cstdlib>
#include <iostream>
using namespace std;


void swapValues(double& val1, double& val2);
void swapValues(int& val1, int& val2);

// An overloaded function has the same name but 
// different parameters

// Void function that outputs value to the screen
// Give a default parameter to val
void outputValue(int val = 0);

// You can have as many defaults as you want
// Defaults start at the end of the header
// can not have a gap
void outputValues( int val1 = 1, int val2 = 2, int val3 = 3);

// Static values
// Static values exist for the length if the program
void counter()
{
    static int num = 0;
    cout << "Number of times ran: " << num << endl;
    num ++;
}



int main() 
{
    int num1 = 5, num2 = 10;
    
    cout << "Before: " << num1 << " " << num2 << endl;
    
    swapValues(num1, num2);
    
    cout << "After: " << num1 << " " << num2 << endl;
    
    double d1 = 3.14, d2 = 1.29;
    
    cout << "Before: " << d1 << " " << d2 << endl;
    
    swapValues(d1, d2);
    
    cout << "After: " << d1 << " " << d2 << endl;
    
    outputValue(12);
    
    // Run the counter function 10 times
    for (int i = 0; i < 10; i ++)
    {
        counter();
    }
    
    
    
    
    
    return 0;
}

void swapValues(double& val1, double& val2)
{
    double temp = val1;
    val1 = val2;
    val2 = temp;
  
}


void swapValues(int& val1, int& val2)
{
    int temp = val1;
    val1 = val2;
    val2 = temp;
}

void outputValue(int val)
{
    cout << "This is the value: " << val << endl;
}

void outputValues( int val1, int val2, int val3)
{
    cout << "These are the values: " << val1;
}