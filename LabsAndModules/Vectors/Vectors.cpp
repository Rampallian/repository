#include <iostream>
#include <cstdlib>
#include <vector>
using namespace std;

int main() 
{
    // A vector is a container
    // A vector is a set of items
    // Need to specify what kind of container the vector is
    
    // SYNTAX:
    // vector<int> v;
    vector<int> v;
    
    // Inserting a value into the vector
    // Use the member function push_back(val)
    // Insert a single value at the end of the vector
    // -----------------------------
    // Begin                     End
    // [][][][][][][][][][][][][][]
    // Insert a few values
    v.push_back(20);
    v.push_back(10);
    v.push_back(100);
    
    // Output the values contained within the vector
    // Access elements of a vector, just like accessing characters of a string
    // e.x. string e = "Hello"; s[2] == 'l'
    cout << v[0] << endl;
    cout << v[2] << endl;
    
    
    // Use a for loop, and output ALL the contents of the vector
    // Use the .size() member function
    cout << "Outputting using a for loop!" << endl;
    for (int i = 0; i < v.size(); i++)
    {
        cout << v[i] << " ";
    }
    
    // Remove a value from a vector
    // Use .pop_back() to remove a single value from a vector
    // Removes the last value
    v.pop_back();
    
    cout << endl;
    cout << "Outputting again after removing last element!" << endl;
    for(int i=0; i < v.size(); i++)   
    {
        cout << v[i] << endl;
    }
    
    return 0;
}

