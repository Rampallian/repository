#include <vector>
#include <cstdlib>
#include <iostream>
using namespace std;

void output(const vector<int>& v);

void deleteUnordered(vector<int>& v, int loc);

void deleteOrdered(vector<int>& v, int loc);



int main() 
{
    vector<int> values;
    
    values.push_back(3);
    values.push_back(4);
    values.push_back(7);
    values.push_back(6);
    values.push_back(12);
    values.push_back(18);
    
    // Output a before and after!
    output(values);
    
    // Delete location 2 which is 7
    deleteUnordered(values, 2);
    
    output(values);
    
    deleteOrdered(values, 2);
    
    output(values);
    
    return 0;
}

void output(const vector<int>& v)
{
    for(int i = 0; i < v.size(); i++)
    {
        cout << v[i] << " ";
    }
    cout << endl;
}

/**
 *  Deletes a value from a given location in a vector
 *  If the location does not exist, no deletion occurs
 *  Uses the swap method
 * @param v Vector of ints
 * @param loc The location you want to delete
 */
void deleteUnordered(vector<int>& v, int loc)
{
    if (loc > 0 && loc < v.size())
    {
        // Valid location!
        // Swap desired location with last location
        //swap(v[loc], v[v.size() - 1]);
        
        int temp = v[loc];
        v[loc] = v[v.size() - 1];
        v[v.size() - 1] = temp;
        
        // The actual delete
        v.pop_back();
    }
    else
    {
        cout << "Invalid location provided!" << endl;
    }
}

/**
 * Deletes a value from a vector given the location
 * Maintains the order of the vector
 * @param v A vector of ints
 * @param loc The location to delete
 */
void deleteOrdered(vector<int>& v, int loc)
{
    // Error checking
    if (loc > 0 && loc < v.size())
    {
        // Shift
        // Make sure to do v.size() - 1
        // Avoid boundary errors
        for (int i = loc; i < v.size() - 1; i++)
        {
            v[i] = v[i + 1];
        }
        
        v.pop_back();
    }
    else
    {
        cout << "Invalid location provided!" << endl;
    }
}
