#include <vector>
#include <cstdlib>
#include <iostream>
using namespace std;

void output(const vector<int>& v);
void add5(vector<int>& v);

// Create a function that returns a function with random values
vector<int> createRandomVector(int size);
void storeRandomValues(vector<int> & v);

int main() 
{
    srand(time(0));
    // Create a vector with 3 random values
    // Use the second way to declare a vector
    vector<int> v; // creates empty vector
    //vector<int> v2(3); // Creates a vector with a size of 3. Contains all 0's
    
    vector<int> v2 = createRandomVector(10000);
    
    // Output the vector before manipulating it
    cout << "Printing out vector before manipulation!" << endl;
    for(int i = 0; i < v2.size(); i++)
    {
        cout << v2[i] << " ";
    }
    cout << endl;
    
    // Increment every value in v2 by 5
    // Use a for loop to iterate through vector
    add5(v2);
    
    // Output the vector after manipulating it
    cout << "Printing out vector after manipulation!" << endl;
    output(v2);
    
    // Change the values to another random set
    storeRandomValues(v2);
    
    cout << endl;
    cout << "New random values: " << endl;
    output(v2);
    
    
    return 0;
}

void output(const vector<int>& v)
{
    for(int i = 0; i < v.size(); i++)
    {
        cout << v[i] << " ";
    }
    cout << endl;
}

/**
 * Adds 5 to every value in the vector
 * @param v - A vector of integers
 */
void add5(vector<int>& v)
{
    for(int i=0; i < v.size(); i++)
    {
        v[i] += 5; 
    }
    
}

/**
 * Creates an returns a vector a random vector
 * with random values between 1 - 100
 * @param size
 * @return A vector with random values
 */
vector<int> createRandomVector(int size)
{
    vector<int> temp; // Empty vector
    
    // Generate a random value
    int high = 20;
    int low = 1;
    
    for (int i = 0; i < size; i++)
    {
        int randValue = rand() % (high - low + 1) + low;
        
        temp.push_back(randValue);
    }
    
    return temp;
}

/**
 * Stores random values in a given vector
 * @param v
 */
void storeRandomValues(vector<int> & v)
{
    int high = 100;
    int low = 20;
    
    for (int i = 0; i < v.size(); i++)
    {
        int randValue = rand() % (high - low + 1) + low;
        v[i] = randValue;
    }
    
}