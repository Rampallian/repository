#include <vector>
#include <cstdlib>
#include <iostream>
using namespace std;

void output(const vector<int>& ids);
void output(const vector<string>& names);
void output(const vector<int>& ids, const vector<string>& names);

int main() 
{
    // Represent students
    vector<int> ids;
    vector<string> names;
    
    // Add a new student
    cout << "Would you like to add a student? ";
    string input;
    cin >> input;
    
    while(tolower(input[0]) == 'y')
    {
        cout << "Please enter a name: ";
        string name;
        cin >> name;
        
        names.push_back(name);
        ids.push_back(ids.size() + 1);
               
        output(ids, names);
        
        cout << "Would you like to add another student? ";
        cin >> input;
    }
    
    
    
    
    
    return 0;
}

void output(const vector<int>& ids)
{
    for(int i = 0; i < ids.size(); i++)
    {
        cout << ids[i] << " ";
    }
    cout << endl;
}

void output(const vector<string>& names)
{
    for(int i = 0; i < names.size(); i++)
    {
        cout << names[i] << " ";
    }
    cout << endl;
}

void output(const vector<int>& ids, const vector<string>& names)
{
    output(ids);
    output(names);
}
