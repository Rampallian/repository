#include <vector>
#include <cstdlib>
#include <iostream>
using namespace std;

// Output function
void output(int a[], int size);

int main() 
{
    //Arrays
    // Syntax:
    // data_type_name[numberOfElements aka size]
    
    char file[20];
    
    int size = 5;
    int a[size];
    
    // Once array is defined, the size can not change
    
    a[0] = 2;
    a[3] = 7;
    
    // To do array manipulation, use a for loop
    for(int i = 0; i < 5; i++)
    {
        //cout << a[i] << " ";
    }
    //cout << endl;
    
    //output(a, size);
    
    // Unable to return an array from function
    //----
    return 0;
}

void output(int a[], int size)
{
    for(int i = 0; i < size; i++)
    {
        cout << a[i] << " ";
    }
    cout << endl;
}