/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: angel
 *
 * Created on September 18, 2021, 8:13 AM
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;

// FILE I/O
// Reading/Writing from a file
// Use fstream library for file IO
// Step 1: Open the file
// Step 2: Use/Manipulate the file
// Step 3: Close the file

int mains() {
    
    // Step 1
    ofstream fout;
    
    // Use the dot op with its member func
    // Opens and creates a new file
    fout.open("test.txt");
    
    // Step 2
    // Use the variable name like cout
    fout << "Hello world!" << endl;
    
    // Step 3
    fout.close();
    
    // Switch Cases
    // Super good for menus
    // Style homework with switch statements
    cout << "1) While Loops" << endl;
    cout << "2) Do-While Loops" << endl;
    cout << "10) For Loops" << endl;
    cout << "3) Exit" << endl;
    
    // Prompt the user
    cout << "Please enter a value: ";
    int input;
    cin >> input;
    
    switch(input) {
        case 1:
            cout << "Running while loops" << endl;
            break;
        case 2:
            cout << "Running do-while loops" << endl;
            break;
        case 10:
            cout << "Running for loop" << endl;
            break;
        default: // basically like else
            cout << "You entered none of the above!" << endl;
            break;
    }
            
    // For loops - Use for known number of loops
    // Syntax:
    // for( start  (declaration); ending condition;
    // output the numbers 1-10 to the user
    // i++  => i + 1;
    for(int i = 0; i < 10; i++) 
    {
        cout << i << endl;
    }
    
    // While loops - Unknown number of loops
    cout << endl << "While Loops!" << endl;
    
    int i = 0;
    while (i < 10) // Condition (end)
    {
        // Increment and code
        cout << i + 1 << " ";
        i++;
    }
    
    // User-controlled loop
    // Get user input for the while loop
    // Count how many times the user decides to loop
    cout << endl;
    cout << "Do you want to run the program? Y/N ";
    string userInput;
    cin >> userInput;
    
    int count = 0;
    
    // grab first character in string and compare it
    while (tolower(userInput[0]) == 'y') {
        // Increase the count
        count++;
        
        cout << "You ran the program " << count << " times!" << endl;
        
        // Re-prompt
        cout << "Would you like to run the program again? Y/N ";
        cin >> userInput;
        
    }
    
    //cout << setw(80) << setfill('-') << "" << setfill('');
    
    //cout << setw(10) << setfill('-') << "" << " " << setw(10) << "" << " " << endl;
    
    //cout << setw(10) << "----" << setw(10) << "----";
    
    cout << setfill('?') << setw(20) << "Hello" << endl;
    
    // Do-While Loop
    // Use for an unknown number of loops
    cout << "Running do-while" << endl;
    
    count = 0;
    
    do {
        count++;
        
        cout << "You have ran the program " << count << " times!" << endl;
        
        cout << "Would you like to run again? ";
        cin >> userInput;
    }
    while (tolower(userInput[0]) == 'y');
    
    string test = "Hello World";
    //for(int i = 0)
    return 0;
}

int main() {
    int userInput;
    for(int i = 0; i < 10; i++) 
    {
        cout << "Feed me a number! " << endl;
        cin >> userInput;
        cout << userInput << endl;
        
    }
    
    string grade;
    
    
    return 0;
}