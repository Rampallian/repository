
#include <cstdlib>
#include <iostream>
using namespace std;


void swapValues(int& val1, int& val2);

int main() 
{

    cout << "Please enter 2 values: ";
    int num1, num2;
    cin >> num1 >> num2;
    
    // SWAP
    cout << "Before: Num1: " << num1 << " Num2: " << num2 << endl;
    
    int temp = num1;
    num1 = num2;
    num2 = temp;
    
    cout << "After: Num1: " << num1 << " Num2: " << num2 << endl;
    
    cout << "\n\nUsing function instead" << endl;
    cout << "Before: Num1: " << num1 << " Num2: " << num2 << endl;
    
    swapValues(num1, num2);
    cout << "After: Num1: " << num1 << " Num2: " << num2 << endl;
    
    
    return 0;
}

/**
 * FUnctions attempts to swap to variables with one another
 * @param val1 An int first value
 * @param val2 An int second value
 */
void swapValues(int& val1, int& val2)
{
    int temp = val1;
    val1 = val2;
    val2 = temp;
    
    
}