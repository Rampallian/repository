/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: angel
 *
 * Created on October 2, 2021, 8:12 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */

// Global Constants are okay
const double PI = 3.14;


// syntax: return_data_type name(parameters)
int square(int number)
{
    return number * number;
}
/**/
/**
 * Calculates whether or not an int is even
 * @param value - value provided by user
 * @return true if the value is even
 */
bool isEven(int value)
{
    return value % 2 == 0;
}

/**
 * Problem 1 calculates whether a value is even or odd
 */
void problem1()
{
    cout << "Please enter a number: ";
    int value;
    cin >> value;
    
    // Output whehter even or odd
    if (value % 2 == 0)
    {
        cout << "Number is even!" << endl;
    }
    else
    {
        cout << "Number is odd!" << endl;
    }
}

// Function prototypes - like declaring a variable
void problem2();

void areaProblem();

double area();

int main()
{
    
        
   
    
    cout << "Enter a number: ";
    int input;
    cin >> input;
    
    // call out square function
    cout << "The square is: " << square(input) << endl;
    
    cout << "1) Problem 1" << endl;
    cout << "2) Problem 2" << endl;
    cout << "3) Area of a Circle" << endl;
    // Create a menu with functions
    int menuOption;
    cin >> menuOption;
    
    switch(menuOption)
    {
        case 1:
            problem1();
            break;           
        case 2:
            problem2();
            break;
        case 3:
            areaProblem();
            break;
        default:
            cout << "No valid input entered..." << endl;
    }
    
    
    return 0;
}

// Define prototype functions here
void problem2()
{
    // Invoke square function with user input
    cout << "Enter a number: ";
    int userInput;
    cin >> userInput;
    
    cout << "The square of " << userInput << " is: "
            << square(userInput) << endl;
}

void areaProblem()
{
    cout << "Enter a radius: " << endl;
    double radius;
    cin >> radius;
    
    cout << "The area is: " << area(radius);
}

double area(double radius)
{
    return radius * 2 * PI;
}
