/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: angel
 *
 * Created on September 11, 2021, 9:47 AM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;

/*
 * 
 */
int main() {
    string var1 = "1";
    int var2 = 2;
    cout << var1 << " + " << var1 << " = " << var1 + var1 << endl;
    cout << var2 << " + " << var2 << " = " << var2 + var2 << endl;
    
    // problem 2
    double convert;
    //cout << "Feed me something to convert! ";
    //cin >> convert;
    
    //cout << convert << endl;
    
    // problem 3
    int singles, doubles, triples, homeruns;
    double atbats, slugging_percentage;
    singles = 40;
    doubles = 30;
    triples = 20;
    homeruns = 10;
    atbats = 1000;
   
    slugging_percentage = (singles + (2 * doubles) + (3 * triples) + (4 * homeruns)) / atbats;
    cout << "The slugging percentage for this player is " << slugging_percentage << endl;
            
    //problem 4
    int x, y, z;
    cin >> x >> y;
    if (y > 1000) {
        cout << "NO NUMBERS OVER 1000" << endl;
    }
    if (x > 1000) {
        cout << "No numbers over 1000" << endl;
    }
            
    cout << "X = " << x << "  Y = " << y << endl;
    
    z = y;
    y = x;
    x = z;
    
    cout << "X = " << x << "  Y = " << y << endl;
    
    //problem 5
    int a = 4;
    int b = 0;
    
    if (a == 4) {
        b = 4;
    }
    else if (x == 9) {
        b = 4;
    }
    else 
    {
        b = 6;
    }
    cout << a << "  " << b << endl;
    return 0;
}

