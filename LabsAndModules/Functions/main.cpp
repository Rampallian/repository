/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: angel
 *
 * Created on October 6, 2021, 3:08 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */

// purpose of func is to determine if the number
// given is even or odd
// return true if num is even
bool isEven(int num)
{
    return num % 2 == 0;
}

double getCircumference(double radius);
double getAreaOfCircle(double radius);

// GLOBAL CONSTANTS
// avoid non global constants aka global variables
// use all caps for constants
const double PI = 3.14;

int main() 
{
    
    
    // get a radius from the user
    // output the circumference of a circle to the user
    cout << "Please enter any value: ";
    double radius;
    cin >> radius;
    
    //double circumference = pi * 2 * radius;
    double circumference = getCircumference(radius);
    
    double area = PI * radius * radius;
    
    cout << "The circumference of the circle is: " << circumference << endl;
    cout <<"The area of the circle is: " << getAreaOfCircle(radius) << endl;
    
    
    return 0;
}

// Function Definitions
/**
 * Gets the circumference of a circle given a radius
 * @param radius  A radius of any value
 * @return Returns the circumference of a circle
 */
double getCircumference(double radius)
{
    return PI * 2 * radius;
}
/**
 * 1) Purpose of function
 * 2) Function parameter description
 * 3) What is being returned
 * Gets area of a circle using the radius
 * @param radius A radius of any value. No error checking
 * @return Returns the circumference of a circle
 */
double getAreaOfCircle(double radius)
{
    return PI * radius * radius;
}

