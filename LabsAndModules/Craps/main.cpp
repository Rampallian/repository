/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: angel
 *
 * Created on September 30, 2021, 3:28 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;


int main() 
{
    // RANDOM NUMBERS
    // Seed a random number
    srand(time(0));
    
    cout << "How many games would you like to play? ";
    int numGames;
    cin >> numGames;
    
    // Counter
    int totalPlayed = 0;
    
    // Variables to keep track of play
    int totalWins = 0;
    int totalLosses = 0;
    
    // The craps game itself
    while (numGames > totalPlayed) 
    {
        cout << "Current game: " << totalPlayed + 1 << endl;
        // Continuation A
        int dice1 = rand() % 6 + 1; // Number 1-6
        int dice2 = rand() % 6 + 1;
        
        int roll1 = dice1 + dice2;
        
        cout << "Roll 1: " << roll1 << endl;
        
        // Flag for winning
        bool won = true;
        
        // Check if won/loss
        if (roll1 == 7 || roll1 == 11)
        {
            won = true;
            totalWins++;
        }
        else if (roll1 == 2 || roll1 == 3 || roll1 == 12)
        {
            won = false;
            totalLosses++;
        }
        else
        {
           // Continuation D
           // Round 2
            dice1 = rand() % 6 + 1;
            dice2 = rand() % 6 + 1;
            int roll2 = dice1 + dice2;
            
            cout << "Roll 2: " << roll2 << endl;
            
            while (roll2 != 7 && roll2 != roll1)
            {
                cout << "Rolling again!" << endl;
                
                dice1 = rand() % 6 + 1;
                dice2 = rand() % 6 + 1;
                roll2 = dice1 + dice2;
                
                cout << "Roll 2: " << roll2 << endl;
                
            }
            if (roll2 == 7)
            {
                won = false;
                totalLosses++;
            }
            else
            {
                won = true;
               totalWins++; 
            }
        }
        // After the game has played
        // End of C
        totalPlayed++;
        
        
        if (won)
        {
            cout << "Won!" << endl;
        }
        else
        {
            cout << "Loss!" << endl;
        }
    }
    
    // Display final result
    cout << "You have won: " << totalWins << endl;
    cout << "You have lost: " << totalLosses << endl;
    
    return 0;
}

