#include <string>
#include <cstdlib>
#include <iostream>
#include <fstream>
using namespace std;

// istream is a dataType that supports input from both files and the user
void readFromInput(istream& input);

int main() 
{
    ofstream out;
    out.open("test.txt", ios::app);
    out << "test file 1 ";
    out.close();
    
    // C-Strings
    // C-Strings are stings that end in a '/0' (null terminator)
    // escaped character '\n'
    
    string x = "Hello World";
    
    // Syntax c-string
    // A set of characters
    // a set is also known as an array
    // Also known as a collection or container
    // ex: File contains 20 possible characters
    // In reality, can only store 19 chars because of null terminator
    char file[20];
    
    cout << "Please input a word: " << endl;
    cin >> file;
    cout << endl;
    
    cout << "You entered: " << file << endl;
    
    cout << "Enter another value: ";
    readFromInput(cin);
    
    
    // Read from the file I wrote at the beginning
    ifstream fin;
    fin.open("test.txt");
    
    cout << endl << endl;
    
    // Error check the file
    if (fin.fail())
    {
        cout << "Reading the file failed. It does not exist!" << endl;
    }
    else
    {
        // Read from the file
        // Use a while loop to read from the file
        while (!fin.eof())
        {
            readFromInput(fin);
        }
    }
    
    fin.close();
    
    // Open the file with flags
    // Use the fstream dataType
    fstream fileStream;
    // ios::in allows input, ios::out allows output
    // ios::app allows appending
    fileStream.open("test.txt", ios::out | ios::in | ios::app);
    fileStream << "Hello I am another line";
    fileStream.close();
    
            
    return 0;
}

// Input is like cin
void readFromInput(istream& input)
{
    string value;
    input >> value;
    
    cout << "The value is: " << value << endl;
}