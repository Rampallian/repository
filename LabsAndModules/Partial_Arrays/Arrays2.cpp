#include <iostream>
#include <cstdlib>

using namespace std;

void output(int a[], int size);

void addValue(int a[], int& size, int capacity, int value);

void removeValue(int a[], int& size);

int main() 
{
    // Partially Filled Arrays
    // NEED:
    // Array
    // Size - Number of elements in array
    // Capacity - Total possible number of elements
    int capacity = 6;
    int size = 0;
    int array[capacity];
    
    output(array, size);
    // Ask user for numbers
    // Output the array after every input
    // Notify the user if the array is filled
    // Give the option to remove the last one
    
    cout << "Would you like to modify the array? Y/N ";
    string input;
    cin >> input;
    
    while (tolower(input[0]) == 'y')
    {
        // Ask the user whether they want to add or remove a value
        // Output initial array to user
        cout << "Current array: ";
        output(array, size);
        
        cout << "Would you like to add or remove a value?" << endl;
        cout << "1) Add a value" << endl;
        cout << "2) Remove a value" << endl;
        
        // Get user input!
        int option;
        cin >> option;
        
        switch(option)
        {
            case 1: // Adding a value
                cout << "Please enter a value: ";
                int value;
                cin >> value;
                
                addValue(array, size, capacity, value);
                
                break;
            case 2:
                // Validate if there are elements to remove
                
                removeValue(array, size);
                
                break;
            default:
                cout << "Not an option!" << endl;
        }
        cout << "After manipulation: ";
        output(array, size);
        
        cout << "Would you like to modify the array again? Y/N ";
        cin >> input;
    }
    
    return 0;
}

// This function outputs any array, partial or non-partial
void output(int a[], int size)
{
    for(int i = 0; i < size; i++)
    {
        cout << a[i] << " ";
    }
    cout << endl;
}

/**
 * Adds a value to the very end of the array aka appending a value
 * @param a The given array
 * @param size The current size of the array
 * @param capacity The total number of possible elements
 */
void addValue(int a[], int& size, int capacity, int value)
{
    if(size < capacity)
    {
        a[size] = value;
        size++; // Increase the number of elements
    }
    else
    {
        cout << "The array is full!" << endl;
    }
}

/**
 * Removes a single value from the end of the partially filled array
 * @param a The given array array
 * @param size THe current size of the array
 */
void removeValue(int a[], int& size)
{
    if(size > 0)
    {
        size--;
    }
    else
    {
        cout << "Array is empty! Nothing to delete." << endl;
    }
}
    