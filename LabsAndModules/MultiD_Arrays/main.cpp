#include <iostream>
#include <cstdlib>

using namespace std;

// global constants
const int COLUMNS = 3;

void assignValues(int a[][COLUMNS], int rows)
{
    // Nested for loops
    // for every single row
    for(int i = 0; i < rows; i++)
    {
        // For every single column within the row
        for(int j = 0; j < rows; j++)
        {
            a[i][j] = i * rows + j + 1;
        }
    }
}

void output(int a[][COLUMNS], int rows)
{
    // Output the array
    for(int i = 0; i < rows; i++)
    {
        // For every single column within the row
        for(int j = 0; j < rows; j++)
        {
            cout << a[i][j] << " ";
        }
        cout << endl;
    }
}

int main() 
{
    // Multi Dimensional Array Syntax
    int rows = 3;
    
    int a[rows][COLUMNS];
    
    assignValues(a, rows);
    
    output(a, rows);
   
    
    return 0;
}

