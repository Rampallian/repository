#include <fstream>
#include <cstdlib>
#include <iostream>
#include <vector>
using namespace std;

int max(int num1, int num2);
int max(int num1, int num2, int num3);
void computeCoins(int coinValue, int& num, int& amountLeft);

void outputVector(vector<int> v);

int main() 
{
    /*
    const int QUARTER = 25;
    const int DIME = 10;
    const int NICKEL = 5;
    const int PENNY = 1;
    const int DOLLAR = 100;
    const int FIVE_DOLLAR = 500;
    
    cout << "How much change?";
    double change;
    cin >> change;
    
    int wholeChange = change * 100;
    
    //computeCoins(FIVE_DOLLAR, num5Dollars, wholeChange);
    //computeCoins(DOLLAR, numDollars, wholeChange);
    */
    
    
    // V E C T O R S
    
    vector<int> v1; //  <- Empty
    vector<int> v2(10); // <- 10 initial elements
    
    // Add a value
    v1.push_back(100); // Adds 100 to the end of the vector
    v1.push_back(50);
    
    
    outputVector(v1);
    outputVector(v2);
    
    // Remove a value from the end of a vector
    v1.pop_back();
    outputVector(v1);
    
    // Write v2 to a file
    ofstream outfile;
    outfile.open("test.txt", ios::app);
    
    for (int i = 0; i < v2.size(); i++)
    {
        outfile << v2[i] << " ";
    }
    outfile << endl;
    
    outfile.close();
    
    return 0;
}

void outputVector(vector<int> v)
{
    for (int i = 0; i < v.size(); i++)
    {
        // Use subscript operator [] to access an element
        cout << v[i] << " ";
        
    }
    cout << endl;
}

int max(int num1, int num2)
{
    if (num1 > num2)
    {
        return num1;
    }
    else
    {
        return num2;      
    }
}

int max(int num1, int num2, int num3)
{
    return max(max(num1, num2), num3);
}

void computeCoins(int coinValue, int& num, int& amountLeft)
{
    num = amountLeft / coinValue;
    amountLeft %= coinValue; //amountLeft -= coinValue * num;
    
    
    
    
}