/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: angel
 *
 * Created on September 30, 2021, 6:02 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main() 
{
// Demorgans law applies to boolean logic
    bool exp1 = 1 > 2;
    bool exp2 = 4 == 4;
    bool exp3 = 3 < 10;
    bool exp4 = 6 != 6;
    
    // false && false
    // exp1 && exp4
    // if (exp1 && exp4);
    // if (1 > 2 && 6 != 6)
    
    // !(false && false)
    // !(false)
    // true
    
    // Distribute the not operator among the things inside parathensis   
    // !false !&& !false) // !&& => ||    !|| => &&
    // true || true
    // true
    
    // !(true && !(false || !true))
    // !(true && !(false || false))
    // !(true && !(false))
    // !(true && true)
    // !(true)
    // false
    
    // !(true && !(false || !true))
    // !(true !&& !!(false || !true))
    // false || (false || false)
    // false
    
    if (!(exp1 && !(exp2 || exp3) || exp4))
    {
        cout << "True!" << endl;
    }
    else
    {
        cout << "False!" << endl;
    }
    
    // (!(exp1 && !(exp2 || exp3) || exp4))
    // !exp1 !&& !!(exp2 || exp3) !|| !exp4
    // !exp1 || (exp2  || exp3) && !exp4
    
    if (!exp1 || (exp2  || exp3) && !exp4)
    {
        cout << "True!" << endl;
   
    }
    else 
    {
        cout << "False!" << endl;
    }
    return 0;
}

