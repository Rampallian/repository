
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;



int main() {
    
    // Problem 1 and 2
    int totalSum, positiveSum, negativeSum;
    int posCounter, negCounter, input;
    double posAverageCounter, negAverageCounter, totalNums;
    int number[10];
    
    posAverageCounter = 0;
    negAverageCounter = 0;
    posCounter = 0;
    negCounter = 0;
    totalNums = 10;
    
    cout << "Please input 10 numbers!" << endl;
    
    for (int i = 0; i < totalNums; i++)
    {
    cout << "Enter a number: ";
    cin >> number[i];
        
        // If number is positive then add number to array/totalSum and and 1 to counter
        if (number[i] > 0)
        {
            posCounter += number[i];
            posAverageCounter ++;
            totalSum += number[i];
        }
        // If number is negative 
        else
        {
            negCounter += number[i];
            negAverageCounter ++;
            totalSum += number[i];
        }
    }

    // Positive, Negative, and total sums
    cout << endl;
    cout << "All the positive numbers add up to " << posCounter << endl;
    cout << "All the negative numbers add up to " << negCounter << endl;
    cout << "All the numbers add up to " << totalSum << endl;
    
    // Averages
    cout << fixed << setprecision(2);
    cout << "The average for all Positive numbers is ";
    cout << posCounter / posAverageCounter << endl;
    cout << "The average for all Negative numbers is ";
    cout << negCounter / negAverageCounter << endl;
    cout << "The average for all numbers is " << totalSum / totalNums << endl;

    // Numbers used
    cout << "The numbers used: " << endl;
    cout << number[0] << " " << number[1] << " " << number[2] 
         << " " << number[3] << " " << number[4] << " " 
         << number[5] << " " << number[6] << " " << number[7]     
         << " " << number[8] << " " << number[9] << endl;
    cout << endl;
    cout << endl;


    // Problem 3 
    int totalSum2, positiveSum2, negativeSum2;
    int posCounter2, negCounter2;
    double posAverageCounter2, negAverageCounter2, totalNums2;
    int number2[10];
    
    posAverageCounter2 = 0;
    negAverageCounter2 = 0;
    posCounter2 = 0;
    negCounter2 = 0;
    totalNums2 = 10;
    totalSum2 = 0;
 
    cout << "10 random numbers from -100 to 100 will now be applied..." << endl;
    
    for (int i = 0; i < totalNums2; i++)
    {
    // variable now equals to a psuedo random number from -100 to 100
    number2[i] = (rand() % 201) - 101;
        
        // If number is positive then add number to array/totalSum and and 1 to counter
        if (number2[i] >= 0)
        {
            posCounter2 += number2[i];
            posAverageCounter2 ++;
            totalSum2 += number2[i];
        }
        // If number is negative 
        else
        {
            negCounter2 += number2[i];
            negAverageCounter2 ++;
            totalSum2 += number2[i];
        }
    }

    // Positive, Negative, and total sums
    cout << endl;
    cout << "All the positive numbers add up to " << posCounter2 << endl;
    cout << "All the negative numbers add up to " << negCounter2 << endl;
    cout << "All the numbers add up to " << totalSum2 << endl;
    
    // Averages
    cout << fixed << setprecision(2);
    cout << "The average for all Positive numbers is ";
    cout << posCounter2 / posAverageCounter2 << endl;
    cout << "The average for all Negative numbers is ";
    cout << negCounter2 / negAverageCounter2 << endl;
    cout << "The average for all numbers is " << totalSum2 / totalNums2 << endl;

    // Numbers used
    cout << "The numbers used: " << endl;
    cout << number2[0] << ", " << number2[1] << ", " << number2[2] 
         << ", " << number2[3] << ", " << number2[4] << ", " 
         << number2[5] << ", " << number2[6] << ", " << number2[7]     
         << ", " << number2[8] << ", " << number2[9] << endl;
    cout << endl;
    cout << endl;

 
    
    // Problem 4
    // Variables and User Prompt/Input
    double weight, height, age, mBMR, fBMR, chocoBars, chocoBarCal;
    string gender, rerun;

    chocoBarCal = 230;

    cout << "Would you like to know your Basal Metabolic Rate Y/N? ";
    cin >> rerun;

    // While loop for Male BMR
    while (tolower(rerun[0]) == 'y') {
        

        cout << "Please enter in your weight in pounds, "
                "your height in inches, and your age: " << endl;
        cout << "Weight: ";
        cin >> weight;
        cout << "Height: ";
        cin >> height;
        cout << "Age: ";
        cin >> age;

        cout << "Now enter M if your a male or F if your a female: ";
        cin >> gender;


        switch (tolower(gender[0])) 
        {
            // Female BMR case
            case 'f':
                // BMR and chocolate bars needed for weight formula
                fBMR = 655 + (4.3 * weight) + (4.7 * height) - (4.7 * age);
                chocoBarCal = 230;
                chocoBars = fBMR / chocoBarCal;

                // Telling user their BMR and how many chocos they need
                // Im going to start eating only chocolate bars for
                // weight maintenence now due to this
                cout << fixed << setprecision(1) << endl;
                cout << "Your Basal Metabolic Rate or BMR is " << fBMR << endl;
                cout << "Therefore, you would need to eat " << 
                        chocoBars << " chocolate bars " << endl;
                cout << "to maintain your current weight without exercising.";
                cout << endl;
                cout << endl;
                break;

            // Male BMR case
            case 'm':
                mBMR = 66 + (6.3 * weight) + (12.9 * height) - (6.8 * age);
                chocoBarCal = 230;
                chocoBars = mBMR / chocoBarCal;

                cout << fixed << setprecision(1) << endl;;
                cout << "Your Basal Metabolic Rate or BMR is " << mBMR << endl;
                cout << "Therefore, you would need to eat " << 
                        chocoBars << " chocolate bars " << endl;
                cout << "to maintain your current weight without exercising.";
                cout << endl;
                cout << endl;
                break;
                
            default:
                cout << "THAT IS AN INVALID INPUT!" << endl;
                cout << endl;
                        
        }
        
        cout << "Do you want to run this program again Y/N? ";
        cin >> rerun;
        
    }
    
    
    
    // Problem 5
    // Variable initials and User input
    double totalExercises, pointsPossible, score; 
    int counter = 0;
    double totalScore = 0;
    double totalPointsPossible = 0;
    double percentage;
    
    cout << endl;
    cout << "How many exercises to input? ";
    cin >> totalExercises;
    cout << endl;
    
    while (totalExercises > counter)
    {
        // Add 1 to counter to symbolize Nth number of exercises
        counter++;
       
        cout << "Score received for exercise " << counter << ": ";
        cin >> score;
        cout << "Total points possible for exercise "  << counter << ": ";
        cin >> pointsPossible;
        cout << endl;   
        
        totalScore = totalScore + score;
        totalPointsPossible = totalPointsPossible + pointsPossible;  
    }   
    
    percentage = totalScore / totalPointsPossible;
    cout << setprecision(0);
    cout << "Your total is " << totalScore << " out of " << totalPointsPossible
            << ", or " << percentage * 100 << "%" << endl;
    
    
    
    //Problem 6 and 7 
    string player1, player2 , toPlayOrNotToPlay;
    
    cout << endl;
    cout << endl;
    cout << "Would you like to play a game of Rock, Paper, Scissors  Y/N? "; 
    cin >> toPlayOrNotToPlay;
    cout << endl;
    
    while (tolower(toPlayOrNotToPlay[0]) == 'y')
    {
        cout << "Player 1, input Rock, Paper, or Scissors: ";
        cin >> player1;
        player1 = tolower(player1[0]);
        cout << "Player 2, input Rock, Paper, or Scissors: ";
        cin >> player2;
        
        // permutations: 
        // Rock:       r vs p (L), r vs s (W), r vs r (N)
        // Paper:      p vs r (W), p vs s (L), p vs p (N)
        // Scissors:   s vs r (L), s vs p (W), s vs s (N)
        
        // If player 1 enters rock then 
        if (tolower(player1[0]) == 'r')
        {
            // If player 2 enters Paper, Rock, or Scissors
            if (tolower(player2[0]) == 'p') // If player 2 enters paper
            {
                cout << "Rock vs Paper - Player 2 wins!" << endl;
            }
            else if (tolower(player2[0]) == 'r') // If player 2 enters rock
            {
                cout << "Rock vs Rock - Nobody wins!" << endl;
            }
            else if (tolower(player2[0]) == 's') // If player 2 enters scissors
            {
                cout << "Rock vs Scissors - Player 1 wins!" << endl;
            }
            else // If player 2 enters in anything else
            {
                cout << "Thats not a valid input!" << endl;
             }
        }
        // If player 1 enters paper        
        else if (tolower(player1[0]) == 'p')
        {
            if (tolower(player2[0]) == 'r')
            {
                cout << "Paper vs Rock - Player 1 wins!" << endl;
            }
            else if (tolower(player2[0]) == 's')
            {
                cout << "Paper vs Scissors - Player 2 wins!" << endl;
            }
            else if (tolower(player2[0]) == 'p')
            {
                cout << "Paper vs Paper - Nobody wins!" << endl;
            }
            else
            {
                cout << "Thats not a valid input!" << endl;
            }
        }
        // If player 1 enters scissors
        else if (tolower(player1[0]) == 's')
        {
            if (tolower(player2[0]) == 'r')
            {
                cout << "Scissors vs Rock - Player 2 wins!" << endl;
            }
            else if (tolower(player2[0]) == 'p')
            {
                cout << "Scissors vs Paper - Player 1 wins!" << endl;
            }
            else if (tolower(player2[0]) == 's')
            {
                cout << "Scissors vs Scissors - Nobody wins!" << endl;
            }
            else
            {
                cout << "Thats not a valid input!" << endl;
            }
        }
        else // If player 1 enters in something other than r, p, or s
        {
            cout << "Thats not a valid input!" << endl;
        }
        
        cout << endl;
        cout << "Would you like to play another game of Rock, Paper, Scissors  Y/N? "; 
        cin >> toPlayOrNotToPlay;
    }
        
    
    return 0;
}

