#include <vector>
#include <cstdlib>
#include <iostream>
using namespace std;

void problem1();
void problem2();
void problem3And4();
void problem5();
void problem6();
void problem7();
void problem8();

void output(const vector<int>& v);
void outputArray(int array[], int size);
void outputArray(char array[], int size);
void insertRandomNumbers(vector<int>& v);
// Credit to your module video explaining this code
// I copied it from that, but I feel I can recreate it, so I hope it's ok to do
// The vector videos have been really well done by the way
void deleteOrdered(vector<int>& v, int loc);

//void deleteArrayLocation()

void vectorDeleteNumber(vector<int>& v);

bool isNumberInVector(const vector<int>& v, int number);

void enterOriginalData(vector<int>& v);

void deleteOrderedArray(int array[], int size);

void delete_repeats(int a[], int size);


int main() 
{
    srand(time(0));
    
    cout << "Homework 8 Menu" << endl;
    cout << "Select a problem to view!" << endl;
    cout << "1) Vector location deletion" << endl;
    cout << "2) Array location deletion" << endl;
    cout << "3) Vector number deletion" << endl;
    cout << "4) Array number deletion" << endl;
    cout << "5) Checking for a number in a vector" << endl;
    cout << "6) Entering in original numbers in a vector" << endl;
    cout << "7) delete_repeats" << endl << endl;
    
    cout << "Enter problem number: ";
    int option;
    cin >> option;
    cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" << endl;
    
    switch(option)
    {
        case 1:
        {
            problem1();
        }
            break;
        case 2:
        {
            problem2();
        }
            break;
        case 3:
        {
            problem3And4();
        }
            break;
        case 4:
        {
            problem5();
        }
            break;
        case 5:
        {
            problem6();
        }
            break;
        case 6:
        {
            problem7();
        }
            break;
        case 7:
        {
            problem8();
        }
            break;    
        default:
        {
            cout << "Invalid input! Exiting..." << endl;
        }
    }
    
    return 0;
}

// Normal Functions ------------------------------------------------------------

/**
 * Deletes a value from a vector given the location
 * Maintains the order of the vector
 * @param v A vector of ints
 * @param loc The location to delete
 */
void deleteOrdered(vector<int>& v, int loc)
{
    // Error checking
    if (loc > 0 && loc < v.size())
    {
        // Shift
        // Make sure to do v.size() - 1
        // Avoid boundary errors
        for (int i = loc; i < v.size() - 1; i++)
        {
            v[i] = v[i + 1];
        }
        
        v.pop_back();
    }
    else
    {
        cout << "Invalid location provided!" << endl;
    }
}

// Outputs a vector
void output(const vector<int>& v)
{
    for(int i = 0; i < v.size(); i++)
    {
        cout << v[i] << " ";
    }
    cout << endl;
}
// Outputs an array
void outputArray(int array[], int size)
{
    for(int i = 0; i < size; i++)
    {
        cout << array[i] << " ";
    }
    cout << endl;
}
void outputArray(char array[], int size)
{
    for(int i = 0; i < size; i++)
    {
        cout << array[i] << " ";
    }
    cout << endl;
}
    
/**
 * Receives a vector and inserts random integers from 1 to 10
 * @param v A vector
 */
void insertRandomNumbers(vector<int>& v)
{
    for (int i = 0; i < v.size(); i++)
    {
        int high = 10;
        int low = 1;
                
        v[i] = rand() % (high - low + 1) + low; 
    }
}

/**
 * Deletes a number given by the user from everywhere within a vector
 * @param v A vector of integers
 * @param number The user given number that needs to be deleted
 */
void vectorDeleteNumber(vector<int>& v)
{
    // Get number to delete from user
    cout << "What number would you like to delete: ";
    int number;
    cin >> number;
    cout << endl;
    
    bool numberFound = false;  

    // Iterate through elements in vector
    for(int i = 0; i < v.size(); i++)
    {
        if (v[i] == number)
        {
            // if the number is found, make it true
            numberFound = true;

            // If number is found then get location of number
            // i is the current iteration, therefor the current index
            // move the index of the number to the end and delete it
            for(int j = i; j < v.size() - 1; j++)
            {
                v[j] = v[j + 1];
            }

            v.pop_back();
            i--;
        }
    }
    // Error checking - Problem 4
    // If numberFound is still false after for loop then the number wasn't found
    if(!numberFound)
    {
        cout << "That value was not found within the vector!" << endl << endl;
    }
}

/**
 * Checks whether or not a specific value is within a given vector
 * @param v An integer vector
 * @param number The value we want to check for
 * @return Returns true if found or false if the value isn't found
 */
bool isNumberInVector(const vector<int>& v, int number)
{   
    for(int i = 0; i < v.size(); i++)
    {
        if(v[i] == number)
        {
            return true;
        }      
    }
    
    return false;
}

/**
 * This function gets a number from the user and checks if the value is in the
 * vector. It stores the value if it hasn't been used and displays an error
 * message if the value is found within the vector already.
 * @param v A vector of ints
 */
void enterOriginalData(vector<int>& v)
{
    string enterAgain;
    
    do
    {
    // Get data value from user
    cout << "Enter a number to the vector: ";
    int input;
    cin >> input;
    
    // Check if input is already in vector
    // If number is found, display error
    if (isNumberInVector(v, input))
    {
        cout << "That number is already being used!" << endl;
    }
    // If number isn't found then add it to vector
    else
    {
        v.push_back(input);
    }
    
    cout << "Vector: ";
    output(v);
    
    cout << endl << "Enter another value Y/N? ";
    cin >> enterAgain;
    cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n" << endl;
    }
    while(tolower(enterAgain[0]) == 'y');
}

/**
 * This function deletes a number from the array while maintaining the order
 * then outputs the array after the deletion
 * @param array AN array of ints
 * @param size The amount of elements within the array
 */
void deleteOrderedArray(int array[], int size)
{
    //output(array, size);
    
    cout << "What number would you like to delete: ";
    int number;
    cin >> number;
    
    for (int i = 0; i < size; i++)
    {
        // if number to be deleted is the same as the element
        if (array[i] == number)
        {
            // start iter on number's index up to one before the size 
            // to avoid boundary error
            for (int j = i; j < (size - 1); j++)
            {
                array[j] = array[j + 1];
            }
        }
    }
    // Print array after changes
    cout << "Array after deletion: " << endl;
    for(int i = 0; i < size - 1; i++)
    {
        cout << array[i] << " ";
    }
    cout << endl;
}

/**
 * This function iterates through a character array and locates repeated
 * characters, then deletes them. It also outputs the array after deletion.
 * @param array A character array
 * @param size The amount of elements within that array
 */
void delete_repeats(char a[], int size)
{
    char current;

    for (int i = 0; i < size; i++)
    {
        // Record current element
        current = a[i];
        
        // Iterate through every other char starting from 1 after the 
        // current element
        for(int j = i + 1; j < size; j++)
        {
            // If the current element is equal to a next up element
            // that element is a repeated element
            if(current == a[j])
            {
                // Send that element striaght to hell and "delete" it
                a[j] = a[j + 1];
                
                // Alternatively, I could just set the repeated element to nothing
                // a[j] = '';  or  a[j] = ' ';
            }
        }
    }
    
    // Output the char array without the last element aka the repeated characters
    cout << "After deletion:" << endl;
    for(int i = 0; i < (size - 1); i++)
    {
        cout << a[i] << " ";
    }
    cout << endl;   
}


// Problem functions -----------------------------------------------------------
//##############################################################################
void problem1()
{
    vector<int> vector1(6);
    insertRandomNumbers(vector1);

    cout << "Vector before: " << endl;
    output(vector1);
    cout << endl;

    deleteOrdered(vector1, 3);

    cout << "Vector after 3rd location deleted: " << endl;
    output(vector1);
    cout << endl;
}

void problem2()
{
    int size = 7;
    int array[size];
    
    for(int i = 0; i < size; i++)
    {
        array[i] = rand() % (10 - 1 + 1) + 1;
    }
    
    outputArray(array, size);
    
    deleteOrderedArray(array, size);
    
    //outputArray(array, size);
    
    cout << endl;
}

void problem3And4()
{
    vector<int> v(10);
    insertRandomNumbers(v);

    cout << "Vector before: " << endl;
    output(v);
    cout << endl;

    // delete 5
    vectorDeleteNumber(v);

    cout << "Vector without the user-given number: " << endl;
    output(v);
}

void problem5()
{
    cout << "oof, I didn't do the problem because im lazy "
            "and am gonna go watch the ufc fight lol" << endl;
}

void problem6()
{
    vector<int> vector(10);
    insertRandomNumbers(vector);
    
    cout << "Vector: ";
    output(vector);
    
    int number = 5;

    cout << endl << "Checking if " << number << " is in the vector..." << endl;
    if(isNumberInVector(vector, number))
    {
        cout << number << " is in the vector!" << endl;
    }
    else
    {
        cout << number << " is NOT in the vector :(" << endl;
    }  
}
    
void problem7()
{
    vector<int> vector;
    
    enterOriginalData(vector);
    
    cout << "Here's your final array:" << endl;
    output(vector);
}

void problem8()
{
    char a[10];
    a[0] = 'a';
    a[1] = 'b';
    a[2] = 'a';
    a[3] = 'c';
    
    
    
    int size = 6;
    
    cout << "Before deletion: " << endl;
    
    outputArray(a, size);
    cout << endl;
    
    delete_repeats(a, size);
    
    cout << "   " << endl;
    cout << endl;
}
