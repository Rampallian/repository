/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: angel
 *
 * Created on September 12, 2021, 7:45 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;

// Problem 3

int main() {
    
    // stating the insane amount of variables I need
    string name1, name2, name3;
    double quiz1a, quiz1b, quiz1c, quiz1d;
    double quiz2a, quiz2b, quiz2c, quiz2d;
    double quiz3a, quiz3b, quiz3c, quiz3d;
    double average1, average2, average3, average4;
    
    // initial prompt and first guys inputs
    cout << "Press ENTER after inputting something!" << endl;
    cout << "Feed me a name and their 4 test scores" << endl;
    cout << "Name: ";
    cin >> name1;
    cout << "Now enter the 4 test scores: " << endl;
    cin >> quiz1a >> quiz1b >> quiz1c >> quiz1d;
    
    // second name
    cout << "Feed me another name and their 4 test scores" << endl;
    cout << "Name: ";
    cin >> name2;
    cout << "Now enter the 4 test scores: " << endl;
    cin >> quiz2a >> quiz2b >> quiz2c >> quiz2d;
    
    // third dude
    cout << "Feed me a third name and their 4 test scores" << endl;
    cout << "Name: ";
    cin >> name3;
    cout << "Now enter the 4 test scores: " << endl;
    cin >> quiz3a >> quiz3b >> quiz3c >> quiz3d;
    
    // average calculashuns
    average1 = (quiz1a + quiz2a + quiz3a) / 3;
    average2 = (quiz1b + quiz2b + quiz3b) / 3;
    average3 = (quiz1c + quiz2c + quiz3c) / 3;
    average4 = (quiz1d + quiz2d + quiz3d) / 3;
    
    // final output
    // Headers
    cout << endl;
    cout << setw(10) << "Name" << setw(10) << "Quiz 1" << setw(10) << "Quiz 2" << setw(10) 
            << "Quiz 3" << setw(10) << "Quiz 4" << endl;
    cout << setw(10) << "----" << setw(10) << "------" << setw(10) << "------" << setw(10) 
            << "------" << setw(10) << "------" << endl;
    
    // Names and scores
    // First guy
    cout << setw(10) << name1 << setw(10) << quiz1a << setw(10) << quiz1b << setw(10) 
            << quiz1c << setw(10) << quiz1d << endl;
    // Second dude
    cout << setw(10) << name2 << setw(10) << quiz2a << setw(10) << quiz2b << setw(10) 
            << quiz2c << setw(10) << quiz2d << endl;
    // Third man
    cout << setw(10) << name3 << setw(10) << quiz3a << setw(10) << quiz3b << setw(10) 
            << quiz3c << setw(10) << quiz3d << endl;
    // Averages
    cout << endl;
    cout << setw(10) << "Average" << setw(10) << fixed << setprecision(2) << average1 <<
            setw(10) << average2 << setw(10) << average3 <<
            setw(10) << average4 << endl;
    cout << endl;
    cout << endl;
    
    
    // Problem 4
    string nameOne, nameTwo, food, number, adj, color, animal;
    
    cout << "Hi! We are going to play a game of Mad Lib." << endl;
    cout << "Please enter the following..." << endl;
    cout << "A name: ";
    cin >> nameOne;
    cout << "Another name: ";
    cin >> nameTwo;
    cout << "A food: ";
    cin >> food;
    cout << " A number between 100 and 120: ";
    cin >> number;   
    cout << "An Adjective: ";
    cin >> adj;
    cout << "A color: ";
    cin >> color;
    cout << "An animal: ";
    cin >> animal;
    
    cout << "\nDear " << nameTwo << "," << endl;
    cout << "\nI am sorry that I am unable to turn in my homework at this time." << endl;
    cout << "First, I ate a rotten " << food << ", which made me turn " << color;
    cout << " and extremely ill.\nI came down with a fever of " << number << ". ";
    cout << "Next, my " << adj << " pet " << animal << " must have " << endl;
    cout << "smelled the remains of the " << food << " on my homework because he";
    cout << " ate it.\nI am currently rewriting my homework and hope you will";
    cout << " accept it late." << endl;
    cout << "\nSincerly," << endl;        
    cout << nameOne << endl;   
    
    return 0;
}