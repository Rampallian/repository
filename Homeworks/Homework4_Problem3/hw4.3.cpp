/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: angel
 *
 * Created on September 30, 2021, 7:15 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main() {
    // write rps game with random variables for the user inputs
    // Polite Menu
    int input;

    cout << "------------------------------------------" << endl;
    cout << "Do you want to play Rock, Paper, Scissors?" << endl;
    cout << "1.) Player vs Player" << endl;
    cout << "2.) Rock, Paper Scissors with AI" << endl;
    cout << "------------------------------------------" << endl;
    cout << "\nWhich would you like to play?" << endl;
    cout << "Enter 1 or 2: ";
    cin >> input;

    switch (input) {
        case 1:
        {
            string player1, player2, toPlayOrNotToPlay;

            cout << endl;
            cout << endl;
            cout << "Would you like to play a game of Rock, Paper, Scissors  Y/N? ";
            cin >> toPlayOrNotToPlay;
            cout << endl;

            while (tolower(toPlayOrNotToPlay[0]) == 'y') {
                cout << "Player 1, input Rock, Paper, or Scissors: ";
                cin >> player1;
                player1 = tolower(player1[0]);
                cout << "Player 2, input Rock, Paper, or Scissors: ";
                cin >> player2;


                if (tolower(player1[0]) == 'r') {
                    if (tolower(player2[0]) == 'p') {
                        cout << "Rock vs Paper - Player 2 wins!" << endl;
                    } else if (tolower(player2[0]) == 'r') {
                        cout << "Rock vs Rock - Nobody wins!" << endl;
                    } else if (tolower(player2[0]) == 's') {
                        cout << "Rock vs Scissors - Player 1 wins!" << endl;
                    } else {
                        cout << "Thats not a valid input!" << endl;
                    }
                }
                else if (tolower(player1[0]) == 'p') {
                    if (tolower(player2[0]) == 'r') {
                        cout << "Paper vs Rock - Player 1 wins!" << endl;
                    } else if (tolower(player2[0]) == 's') {
                        cout << "Paper vs Scissors - Player 2 wins!" << endl;
                    } else if (tolower(player2[0]) == 'p') {
                        cout << "Paper vs Paper - Nobody wins!" << endl;
                    } else {
                        cout << "Thats not a valid input!" << endl;
                    }
                } else if (tolower(player1[0]) == 's') {
                    if (tolower(player2[0]) == 'r') {
                        cout << "Scissors vs Rock - Player 2 wins!" << endl;
                    } else if (tolower(player2[0]) == 'p') {
                        cout << "Scissors vs Paper - Player 1 wins!" << endl;
                    } else if (tolower(player2[0]) == 's') {
                        cout << "Scissors vs Scissors - Nobody wins!" << endl;
                    } else {
                        cout << "Thats not a valid input!" << endl;
                    }
                } else {
                    cout << "Thats not a valid input!" << endl;
                }

                cout << endl;
                cout << "Would you like to keep playing Rock, Paper, Scissors Y/N? ";
                cin >> toPlayOrNotToPlay;
            }
            break;
        }
        case 2:
        {
            // RPS but with AI
            string toPlayOrNotToPlay;
            int player1AI, player2AI;

            cout << endl;
            cout << "Would you like to see a game of AI Rock, Paper, Scissors Y/N? ";
            cin >> toPlayOrNotToPlay;
            cout << endl;

            while (tolower(toPlayOrNotToPlay[0]) == 'y') 
            {
                player1AI = (rand() % 3) + 1;
                player2AI = (rand() % 3) + 1;
                cout << "p1 = " << player1AI << "   p2 = " << player2AI << endl;
                
                // rock = 1 paper = 2 scissors = 3
                // If player 1 rolled a 1(rock)
                if (player1AI == 1) 
                {
                    // If player 2 rolled a 2(paper)
                    if (player2AI == 2) 
                    {
                        cout << "Rock vs Paper - Player 2 wins!" << endl;
                    } 
                    // If player 2 rolled a 1(rock)
                    else if (player2AI == 1) 
                    {
                        cout << "Rock vs Rock - Nobody wins!" << endl;
                    } 
                    // If player 2 rolled a 3(scissors)
                    else if (player2AI == 3) 
                    {
                        cout << "Rock vs Scissors - Player 1 wins!" << endl;
                    } 
                    else 
                    {
                        cout << "Thats not a valid input!" << endl;
                    }
                }
                // If player 1 rolled a 2(paper)
                else if (player1AI == 2) 
                {
                    if (player2AI == 1) 
                    {
                        cout << "Paper vs Rock - Player 1 wins!" << endl;
                    }
                    else if (player2AI == 3) 
                    {
                        cout << "Paper vs Scissors - Player 2 wins!" << endl;
                    }
                    else if (player2AI == 2) 
                    {
                        cout << "Paper vs Paper - Nobody wins!" << endl;
                    }
                    else 
                    {
                        cout << "Thats not a valid input!" << endl;
                    }
                } 
                // If player 2 rolled a 3(scissors)
                else if (player1AI == 3) 
                {
                    if (player2AI == 1) 
                    {
                        cout << "Scissors vs Rock - Player 2 wins!" << endl;
                    }
                    else if (player2AI == 2) 
                    {
                        cout << "Scissors vs Paper - Player 1 wins!" << endl;
                    }
                    else if (player2AI == 3) 
                    {
                        cout << "Scissors vs Scissors - Nobody wins!" << endl;
                    }
                    else 
                    {
                        cout << "Thats not a valid input!" << endl;
                    }
                } 
                else 
                {
                    cout << "Thats not a valid input!!!" << endl;
                }
                // Ask user if they want to generate another game
                cout << endl;
                cout << "Would you like to play another game of Rock, Paper, Scissors  Y/N? ";
                cin >> toPlayOrNotToPlay;
            }
            // Make sure break is outside of the while loop
            break;
        }
        default:
        {
            cout << "Invalid input...  Exiting..." << endl;
        }
    }



    return 0;
}

