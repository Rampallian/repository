/**  Name: Angel Medina
 *   Student ID: 2933255
 *   Date: 9/11/2021
 *   HW: Homework 1
 *   Problem: all of them 
 *   I certify this is my own work and code*/

#include <cstdlib>
#include <iostream>
#include<bits/stdc++.h>

using namespace std;


int main() {

    
    // adding two variables
    
    int x = 50;
    int y = 100;
    int totall = x + y;
    cout << "50 and 100 added together equal " << totall << "\n" << endl;

// sales tax question
    
    double sales_tax = .06;
    double purchase = 95;
    double total = purchase + (sales_tax * purchase);
    cout << "Total sales tax: " << total << "\n" << endl;

// restaurant bill question

    double mealCost, salesTax, tipAmount, bill, temp, temp2;
    mealCost = 88.67;
    salesTax = .0675;
    tipAmount = .2;
    temp = mealCost + (mealCost * salesTax);
    temp2 = tipAmount * temp;
    bill = temp + temp2;
    cout << "Meal cost = " << mealCost << endl;
    cout << "Sales tax = " << salesTax << endl;
    cout << "Tip amount = " << tipAmount << endl;
    cout << "The total restaurant bill is " << fixed << setprecision(2) << bill << "\n" << endl;

// M P G

    int mpg, miles, gallons;
    miles = 375;
    gallons = 15;
    mpg = miles / gallons;
    cout << "The miles per gallon the car gets is " << mpg << "\n" << endl;
    
// personal info

    cout << "Angel Medina\n" << "(909)420-1337\n" << "Computer Science\n" << endl;

// question 6. 1-4

    int number_of_pods, peas_per_pod, total_peas;
    cout << "Hello\n";
    cout << "Press return after entering a number.\n";
    cout << "Enter the number of pods:\n";
    
    cin >> number_of_pods;
    
    cout << "Enter the number of peas in a pod:\n";
    cin >> peas_per_pod;
    total_peas = number_of_pods + peas_per_pod;
    cout << "If you have ";
    cout << number_of_pods;
    cout << " pea pods\n";
    cout << "and ";
    cout << peas_per_pod;
    cout << " peas in each pod, then\n";
    cout << "you have ";
    cout << total_peas;
    cout << " peas in all the pods.\n";
    cout << "Good Bye" << endl;
    
    // program adding and multiplying two numbers
    
    int ex, why;
    cout << "\nFeed me two numbers!: " << endl;
    cin >> ex >> why;
    cout << ex << " + " << why << " = " << ex + why <<
            " and " << ex << " * " << why << " = " << ex * why << endl;
    
   
    return 0;
}

