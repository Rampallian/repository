#include <vector>
#include <cstdlib>
#include <iostream>
#include <fstream>
using namespace std;

void insertRandomNumbers(vector<int>& v);

void output(const vector<int>& v);

int countOddIntegers(const vector<int>& v);

int main() 
{
    srand(time(0));
    // Problem 1 and 2
    
    ifstream file;
    file.open("data.dat", ios::in | ios::out);
    
    int currentNum, largest, smallest;
    largest = smallest = currentNum;
    
    cout << "The numbers in the file: " << endl;
    // Error check the file
    if (file.fail())
    {
        cout << "Reading the file failed. It does not exist!" << endl;
    }
    else
    {
        while (!file.eof())
        {
            // set the files "element" to variable
            file >> currentNum;
            cout << currentNum << " ";
 
            // if current number in file is larger than largest
            if(currentNum > largest)
            {
                largest = currentNum;
            }
            // if current number is less than current smallest number   
            if(currentNum < smallest)
            {
                smallest = currentNum;
            } 
        }
    }
    cout << endl << endl;         
    cout << "The largest number is: " << largest << endl;
    cout << "The smallest number is: " << smallest << endl << endl;
    
    file.close();
    
    
    // Problem 3
    // Creating each file
    ofstream out;
    out.open("data1.dat");
    out << "1 1 1 2 1 1 2 1 2 2 1 2";
    out.close();
    
    out.open("data2.dat");
    out << "1 3 3 9 7 77 3 13 19 45 5 1 23";
    out.close();
    
    out.open("data3.dat");
    out << "1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20";
    out.close();
    
    ifstream file1, file2, file3;
    file1.open("data1.dat");
    file2.open("data2.dat");
    file3.open("data3.dat");
    int current, evenCounter1 = 0, evenCounter2 = 0, evenCounter3 = 0;
    
    cout << "File 1 data: " << endl;
    if (file1.fail())
    {
        cout << "Reading the file failed. It does not exist!" << endl;
    }
    else
    {
        while (!file1.eof()) // read till End Of File
        {
            file1 >> current; // set current number to current
            cout << current << " ";
            // if current number divides cleanly with no remainder, then its even
            if(current % 2 == 0)
            {
                evenCounter1++;
            }
        }
    }
    cout << endl;
    cout << "File 1 contains " << evenCounter1 << " even numbers." << endl << endl;
    file1.close();
    //--------------------------------------------------------------------------
    cout << "File 2 data: " << endl;
    
    if (file2.fail())
    {
        cout << "Reading the file failed. It does not exist!" << endl;
    }
    else
    {
        while (!file2.eof()) // read till End Of File
        {
            file2 >> current; // set current number to current
            cout << current << " ";
            // if current number divides cleanly with no remainder, then its even
            if(current % 2 == 0)
            {
                evenCounter2++;
            }
        }
    }
    cout << endl;
    cout << "File 2 contains " << evenCounter2 << " even numbers." << endl << endl;
    file2.close();
    //--------------------------------------------------------------------------
    cout << "File 3 data: " << endl;
    
    if (file3.fail())
    {
        cout << "Reading the file failed. It does not exist!" << endl;
    }
    else
    {
        while (!file3.eof()) // read till End Of File
        {
            file3 >> current; // set current number to current
            cout << current << " ";
            // if current number divides cleanly with no remainder, then its even
            if(current % 2 == 0)
            {
                evenCounter3++;
            }
        }
    }
    cout << endl;
    cout << "File 3 contains " << evenCounter3 << " even numbers." << endl << endl;
    cout << endl << endl;;
    
    
    file3.close();
    
    
    // Problem 4
    ofstream create;
    create.open("middle.dat");
    create << "1 3 4 6 8 12 16 17 20 21 26 29 30 42 45 46 49 55 58 59 60 69 420";
    create.close();
    
    // Creating and opening file
    ifstream middle;
    middle.open("middle.dat");
    vector<int> data;
    int elementCounter = 0, middleFinder = 0;
    int median;
    
    if (middle.fail())
    {
        cout << "Reading the file failed. It does not exist!" << endl;
    }
    else
    {
        while (!middle.eof())
        {
            // Display each number
            middle >> current;
            cout << current << " ";
            // At every number, add 1 to counter
            elementCounter++;
            // Add element to vector
            data.push_back(current);
        }
    }
    middle.close();
    cout << endl;
    cout << "Number of elements in file: " << elementCounter << endl;
        
    //int median = data[(data.size() / 2)];
    
    // if the amount of numbers in file is even
    if (elementCounter % 2 == 0) 
    {
        // divide total by 2 and go back one element and add the next
        // element after that, then divide that number by two for the average
        median = (data[data.size() / 2 - 1] + data[data.size() / 2]) / 2;
        cout << endl << "Median: " << median << endl;
    }
    // if odd
    else 
    {
        // the median is the middle number, so just divide by two
	median = data[data.size() / 2];
	cout << "Median: " << median << endl;
    }
    cout << endl << endl;
    
    
    // Problem 5 and 6
    vector<int> vector(10);
    
    insertRandomNumbers(vector);
    
    output(vector);

    cout << "The number of odd integers is " << countOddIntegers(vector) << endl;
    
    return 0;
}

/**
 * Receives a vector and inserts random integers from 1 to 20 
 * @param v A vector
 */
void insertRandomNumbers(vector<int>& v)
{
    for (int i = 0; i < v.size(); i++)
    {
        int high = 20;
        int low = 1;
                
        v[i] = rand() % (high - low + 1) + low; 
    }
}

// Outputs a vector
void output(const vector<int>& v)
{
    for(int i = 0; i < v.size(); i++)
    {
        cout << v[i] << " ";
    }
    cout << endl;
}

/**
 * This function counts the number of odd numbers within a vector
 * @param v Just a vector
 * @return The number of odd integers
 */
int countOddIntegers(const vector<int>& v)
{
    // Odd integer counter
    int odds = 0;
    
    for (int i = 0; i < v.size(); i++)
    {
        // if element doesn't fully divide into 2 then its odd
        if(v[i] % 2 == 1)
        {
            // if odd then add 1 to counter
            odds++;
        }
    }
    
    return odds;
}
