/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: angel
 *
 * Created on September 29, 2021, 1:54 PM
 */

#include <cstdlib>
#include <iostream>
#include <algorithm>
#include <string>
#include <fstream>
using namespace std;


void problem3();

void problem4And7();

void problem5();


int main() 
{
    // Main HW menu
    cout << "Welcome to my Homework 4!" << endl;
    cout << "-------------------------" << endl;
    cout << "1.) Problem 3" << endl;
    cout << "2.) Problem 4 and 7" << endl;
    cout << "3.) Problem 5" << endl;
    cout << "-------------------------" << endl;
    cout << endl;
    cout << "Enter the number of the problem you'd like to see: ";
    int option;
    cin >> option;
    
    switch(option)
    {
        case 1:
            problem3();
            break;
        case 2:
            problem4And7();
            break;
        case 3:
            problem5();
            break;
        default:
            cout << "Invalid Input... Exiting..." << endl;
    }
    return 0;
}





// Functions

void problem3()
{
    // write rps game with random variables for the user inputs
    // Polite Menu
    int input;

    cout << "------------------------------------------" << endl;
    cout << "Do you want to play Rock, Paper, Scissors?" << endl;
    cout << "1.) Player vs Player" << endl;
    cout << "2.) Rock, Paper Scissors with AI" << endl;
    cout << "------------------------------------------" << endl;
    cout << "\nWhich would you like to play?" << endl;
    cout << "Enter 1 or 2: ";
    cin >> input;

    switch (input) {
        case 1:
        {
            string player1, player2, toPlayOrNotToPlay;

            cout << endl;
            cout << endl;
            cout << "Would you like to play a game of Rock, Paper, Scissors  Y/N? ";
            cin >> toPlayOrNotToPlay;
            cout << endl;

            while (tolower(toPlayOrNotToPlay[0]) == 'y') 
            {
                cout << "Player 1, input Rock, Paper, or Scissors: ";
                cin >> player1;
                player1 = tolower(player1[0]);
                cout << "Player 2, input Rock, Paper, or Scissors: ";
                cin >> player2;


                if (tolower(player1[0]) == 'r') 
                {
                    if (tolower(player2[0]) == 'p') 
                    {
                        cout << "Rock vs Paper - Player 2 wins!" << endl;
                    } 
                    else if (tolower(player2[0]) == 'r') 
                    {
                        cout << "Rock vs Rock - Nobody wins!" << endl;
                    } 
                    else if (tolower(player2[0]) == 's') 
                    {
                        cout << "Rock vs Scissors - Player 1 wins!" << endl;
                    } 
                    else 
                    {
                        cout << "Thats not a valid input!" << endl;
                    }
                }
                else if (tolower(player1[0]) == 'p') 
                {
                    if (tolower(player2[0]) == 'r') 
                    {
                        cout << "Paper vs Rock - Player 1 wins!" << endl;
                    } 
                    else if (tolower(player2[0]) == 's') 
                    {
                        cout << "Paper vs Scissors - Player 2 wins!" << endl;
                    } 
                    else if (tolower(player2[0]) == 'p') 
                    {
                        cout << "Paper vs Paper - Nobody wins!" << endl;
                    } 
                    else 
                    {
                        cout << "Thats not a valid input!" << endl;
                    }
                } 
                else if (tolower(player1[0]) == 's') 
                {
                    if (tolower(player2[0]) == 'r') 
                    {
                        cout << "Scissors vs Rock - Player 2 wins!" << endl;
                    } 
                    else if (tolower(player2[0]) == 'p') 
                    {
                        cout << "Scissors vs Paper - Player 1 wins!" << endl;
                    } 
                    else if (tolower(player2[0]) == 's') {
                        cout << "Scissors vs Scissors - Nobody wins!" << endl;
                    } 
                    else 
                    {
                        cout << "Thats not a valid input!" << endl;
                    }
                } else {
                    cout << "Thats not a valid input!" << endl;
                }

                cout << endl;
                cout << "Would you like to keep playing Rock, Paper, Scissors Y/N? ";
                cin >> toPlayOrNotToPlay;
            }
            break;
        }
        case 2:
        {
            // RPS but with AI
            string toPlayOrNotToPlay;
            int player1AI, player2AI;

            cout << endl;
            cout << "Would you like to see a game of AI Rock, Paper, Scissors Y/N? ";
            cin >> toPlayOrNotToPlay;
            cout << endl;

            while (tolower(toPlayOrNotToPlay[0]) == 'y') 
            {
                // Put random number in a variable
                player1AI = (rand() % 3) + 1;
                player2AI = (rand() % 3) + 1;
                cout << "p1 = " << player1AI << "   p2 = " << player2AI << endl;
                
                // rock = 1 paper = 2 scissors = 3
                // If player 1 rolled a 1(rock)
                if (player1AI == 1) 
                {
                    // If player 2 rolled a 2(paper)
                    if (player2AI == 2) 
                    {
                        cout << "Rock vs Paper - Player 2 wins!" << endl;
                    } 
                    // If player 2 rolled a 1(rock)
                    else if (player2AI == 1) 
                    {
                        cout << "Rock vs Rock - Nobody wins!" << endl;
                    } 
                    // If player 2 rolled a 3(scissors)
                    else if (player2AI == 3) 
                    {
                        cout << "Rock vs Scissors - Player 1 wins!" << endl;
                    } 
                    else 
                    {
                        cout << "Thats not a valid input!" << endl;
                    }
                }
                // If player 1 rolled a 2(paper)
                else if (player1AI == 2) 
                {
                    if (player2AI == 1) 
                    {
                        cout << "Paper vs Rock - Player 1 wins!" << endl;
                    }
                    else if (player2AI == 3) 
                    {
                        cout << "Paper vs Scissors - Player 2 wins!" << endl;
                    }
                    else if (player2AI == 2) 
                    {
                        cout << "Paper vs Paper - Nobody wins!" << endl;
                    }
                    else 
                    {
                        cout << "Thats not a valid input!" << endl;
                    }
                } 
                // If player 2 rolled a 3(scissors)
                else if (player1AI == 3) 
                {
                    if (player2AI == 1) 
                    {
                        cout << "Scissors vs Rock - Player 2 wins!" << endl;
                    }
                    else if (player2AI == 2) 
                    {
                        cout << "Scissors vs Paper - Player 1 wins!" << endl;
                    }
                    else if (player2AI == 3) 
                    {
                        cout << "Scissors vs Scissors - Nobody wins!" << endl;
                    }
                    else 
                    {
                        cout << "Thats not a valid input!" << endl;
                    }
                } 
                else 
                {
                    cout << "Thats not a valid input!!!" << endl;
                }
                // Ask user if they want to generate another game
                cout << endl;
                cout << "Would you like to play another game of Rock, Paper, Scissors  Y/N? ";
                cin >> toPlayOrNotToPlay;
            }
            // Make sure break is outside of the while loop
            break;
        }
        default:
        {
            cout << "Invalid input...  Exiting..." << endl;
        }
    }

}

void problem4And7() 
{
    // Problem 4 and 7
     ifstream fin;
     fin.open("data.dat");
   
    
    double numLiters, miles, mpg, gallons;
    double liter = 0.264179;
    double numLiters2, miles2, mpg2, gallons2;
    
    // Car 1
    cout << "Liters car 1 holds: ";
    fin >> numLiters;
    cout << numLiters << endl;
    cout << "Miles car 1 has traveled: ";
    fin >> miles;
    cout << miles << endl;
    // Car 2
    cout << "Liters car 2 holds: ";
    fin >> numLiters2;
    cout << numLiters2 << endl;
    cout << "Miles car 2 has traveled: ";
    fin >> miles2;
    cout << miles2 << endl;
    
    // In the future I can use a function here
    // Liter to gallons conversion
    gallons = numLiters * liter;
    // Miles per gallon conversion
    mpg = miles / gallons;
    
    gallons2 = numLiters2 * liter;
    mpg2 = miles2 / gallons2;
    
    cout << "The miles per gallon car 1 gets is: " << mpg << endl;
    cout << "The miles per gallon car 2 gets is: " << mpg2 << endl;
    
    // Comparing the two cars
    if (mpg > mpg2)
    {
        cout << "Car 1 gets better mileage!" << endl;
    }
    else
    {
        cout << "Car 2 gets better mileage!" << endl;
    }
    fin.close(); 
    cout << endl;
    cout << endl;
    
}

void problem5() 
{
    // Problem 5
    // if '-' is found in string then kill it
    
    cout << "Please enter in your phone number: ";
    string phonenum;
    cin >> phonenum;
    
    for (int i=0; i < phonenum.size(); i++)
    {
        // Solution coming soon...
        
    }
    
}