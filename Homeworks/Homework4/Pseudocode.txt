Homework 4 Pseudocode

Problem 2 - Pseudocode for Rock, Paper, Scissors

ask the user if they would like to play
store their input
while their input is yes
   get player 1's input of either Rock, Paper, or Scissors
   store their input in player1
   get player 2's input of R, P, or S
   store their input in player2
   
   if player 1 entered rock
      if player 2 entered paper
	 output player 2 wins
      if player 2 entered scissors
	 output player 1 wins
      if player 2 entered rock
	 output that nobody won
   if player 1 entered paper
      if player 2 entered scissors
	 output player 2 wins
      if player 2 entered rock
	 output player 1 wins
      if player 2 entered paper
	 output that nobody won
   if player 1 entered scissors
      if player 2 entered rock
	 output player 2 wins
      if player 2 entered paper
	 output player 1 wins
      if player 2 entered scissors
	 output that nobody won
   else output that the input is invalid
   ask the user if they want to play another game
   if input is no then the while loop breaks


Problem 7 - MPG Data.dat

open the data.dat file
initialize the variables for the number of liters,
miles, mpg, and gallons

cout liters car takes in
take the liters from file
cout miles the car traveled
get miles from file

convert the liters to gallons
divide the miles by gallons to get mpg

output the mpg
close the file
   