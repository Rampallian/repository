
#include <cstdlib>
#include <iostream>
#include <string>
#include <iomanip>
using namespace std;

// FUNCTIONS
void userRockPaper();
void AIRockPaper();

double MPG(double miles, double gallons);

void iceCreamPortion();

double conversionGetPounds();
double conversionCalc(double pound);
void conversionOutput(double grams);

// CONSTANT VARIABLES
const double LITER = 0.264179;

int main() 
{
    cout << "|------------------------------------------|" << endl;
    cout << "|Input the question number you want to see!|" << endl;
    cout << "|1.) Problem 1 - User or AI Rock Paper     |" << endl;
    cout << "|2.) Problem 2 - MPG conversion            |" << endl;
    cout << "|3.) Problem 3 - Ice Cream Portions        |" << endl;
    cout << "|4.) Problem 6 - Imperial to Metric        |" << endl;
    cout << "|------------------------------------------|" << endl;
    cout << "Input here: ";
    int userInput;
    cin >> userInput;
    cout << endl;
    cout << endl;
    
    
    switch(userInput)
    {
        case 1: 
        {
            // Problem 1 and 5
            
            cout << "Which game would you like to run?" << endl;
            cout << "1.) User vs User" << endl;
            cout << "2.) AI" << endl;
            cout << "Input either 1 or 2: ";
            int input;
            cin >> input;
            
            switch(input)
            {
                case 1:
                    userRockPaper();
                    break;
                case 2:
                    AIRockPaper();
                    break;
            }
        break; 
        }  
        case 2: 
        {   
            // Problem 2
            string runTheLoop;
            do 
            {
                cout << "\nHow many liters does your car hold: ";
                double liters;
                cin >> liters;
                cout << "How many miles did your car travel: ";
                double miles;
                cin >> miles;
                
                cout << "\nYour car gets " << MPG(miles, liters) << " MPG" << endl;
                
                cout << "Would you like to run this program again? Y/N ";
                cin >> runTheLoop;
            }
            while(tolower(runTheLoop[0]) == 'y');
            
            break;
        }
        case 3:
        {
            // Problem 3
            iceCreamPortion();
            break;
        }
        case 4:
        {
            // Problem 5
            string runTheLoop;
            do
            {
                conversionOutput(conversionCalc(conversionGetPounds()));
                
            
                cout << "Would you like to run this program again? Y/N ";
                cin >> runTheLoop;
            }
            while(tolower(runTheLoop[0]) == 'y');
            break;
        }
        default:
            cout << "That's an invalid input! Exiting..." << endl;
    }     
    return 0;
}




/**
 * Asks player 1 and player 2 for their choice of rock, paper, or scissors.
 * Then compares player 1's option to player 2's and outputs who won
 * based on the choices. Lastly, it asks if they'd like to play again.
 * @param value No parameters
 * @return No
 */
void userRockPaper() 
{
    string player1, player2, toPlayOrNotToPlay;

    do 
    { 
        cout << endl;
        cout << "Player 1, input Rock, Paper, or Scissors: ";
        cin >> player1;
        player1 = tolower(player1[0]);
        cout << "Player 2, input Rock, Paper, or Scissors: ";
        cin >> player2;


        if (tolower(player1[0]) == 'r') 
        {
            if (tolower(player2[0]) == 'p') 
            {
                cout << "Rock vs Paper - Player 2 wins!" << endl;
            } 
            else if (tolower(player2[0]) == 'r') 
            {
                cout << "Rock vs Rock - Nobody wins!" << endl;
            } 
            else if (tolower(player2[0]) == 's') 
            {
                cout << "Rock vs Scissors - Player 1 wins!" << endl;
            } 
            else 
            {
                cout << "Thats not a valid input!" << endl;
            }
        }
        else if (tolower(player1[0]) == 'p') 
        {
            if (tolower(player2[0]) == 'r') 
            {
                cout << "Paper vs Rock - Player 1 wins!" << endl;
            } 
            else if (tolower(player2[0]) == 's') 
            {
                cout << "Paper vs Scissors - Player 2 wins!" << endl;
            } 
            else if (tolower(player2[0]) == 'p') 
            {
                cout << "Paper vs Paper - Nobody wins!" << endl;
            } 
            else 
            {
                cout << "Thats not a valid input!" << endl;
            }
        } 
        else if (tolower(player1[0]) == 's') 
        {
            if (tolower(player2[0]) == 'r') 
            {
                cout << "Scissors vs Rock - Player 2 wins!" << endl;
            } 
            else if (tolower(player2[0]) == 'p') 
            {
                cout << "Scissors vs Paper - Player 1 wins!" << endl;
            } 
            else if (tolower(player2[0]) == 's') {
                cout << "Scissors vs Scissors - Nobody wins!" << endl;
            } 
            else 
            {
                cout << "Thats not a valid input!" << endl;
            }
        } 
        else 
        {
            cout << "Thats not a valid input!" << endl;
        }

        cout << endl;
        cout << "Would you like to keep playing Rock, Paper, Scissors Y/N? ";
        cin >> toPlayOrNotToPlay;
    }
    while (tolower(toPlayOrNotToPlay[0]) == 'y');
}

/**
 * Outputs the result of a "AI" generated RockPaperScissors game
 * what both players chose, who won or lost, and if they'd like to run again
 * @param value Receives no value
 * @return No return
 */
void AIRockPaper() 
{   
    string toPlayOrNotToPlay;
    do
    {
        
        
        int player1AI, player2AI;

        srand(time(0));
        player1AI = (rand() % 3) + 1;
        player2AI = (rand() % 3) + 1;

        cout << endl;
        cout << "Player 1 = " << player1AI << "   Player 2 = " << player2AI << endl;
        
        // rock = 1 paper = 2 scissors = 3
        // If player 1 rolled a 1(rock)
        if (player1AI == 1) 
        {
            // If player 2 rolled a 2(paper)
            if (player2AI == 2) 
            {
                cout << "Rock vs Paper - Player 2 wins!" << endl;
            } 
            // If player 2 rolled a 1(rock)
            else if (player2AI == 1) 
            {
                cout << "Rock vs Rock - Nobody wins!" << endl;
            } 
            // If player 2 rolled a 3(scissors)
            else if (player2AI == 3) 
            {
                cout << "Rock vs Scissors - Player 1 wins!" << endl;
            } 
            else 
            {
                cout << "Thats not a valid input!" << endl;
            }
        }
        // If player 1 rolled a 2(paper)
        else if (player1AI == 2) 
        {
            if (player2AI == 1) 
            {
                cout << "Paper vs Rock - Player 1 wins!" << endl;
            }
            else if (player2AI == 3) 
            {
                cout << "Paper vs Scissors - Player 2 wins!" << endl;
            }
            else if (player2AI == 2) 
            {
                cout << "Paper vs Paper - Nobody wins!" << endl;
            }
            else 
            {
                cout << "Thats not a valid input!" << endl;
            }
        } 
        // If player 2 rolled a 3(scissors)
        else if (player1AI == 3) 
        {
            if (player2AI == 1) 
            {
                cout << "Scissors vs Rock - Player 2 wins!" << endl;
            }
            else if (player2AI == 2) 
            {
                cout << "Scissors vs Paper - Player 1 wins!" << endl;
            }
            else if (player2AI == 3) 
            {
                cout << "Scissors vs Scissors - Nobody wins!" << endl;
            }
            else 
            {
                cout << "Thats not a valid input!" << endl;
            }
        } 
        else 
        {
            cout << "Thats not a valid input!!!" << endl;
        }
        
        // Ask user if they want to generate another game
        cout << endl;
        cout << "Would you like to play another game of Rock, Paper, Scissors  Y/N? ";
        cin >> toPlayOrNotToPlay;
    }
    while (tolower(toPlayOrNotToPlay[0]) == 'y');   
}

/**
 * Converts the liters to gallons then gets the miles per gallon
 * @param value Takes in the users miles traveled and liters their car holds
 * @return Returns the miles divided by the gallons
 */
double MPG(double miles, double liters)
{
    // converting liters to gallons
    double gallons = liters * LITER;
    // return miles per gallon
    return miles / gallons;
}

void iceCreamPortion()
{
    cout << "How many customers are there: ";
    double customers;
    cin >> customers;

    cout << "How many ounces of ice cream is there: ";
    double weight;
    cin >> weight;

    cout << "The portion size for all " << customers << " customers is ";
    cout << weight / customers << " ounces of ice cream." << endl;
}

// Literally just getting the user input and returning them
double conversionGetPounds()
{
    cout << endl;
    cout << endl;
    cout << "Input the number of pounds and/or ounces you want to convert!" << endl;
    cout << "Number of pounds: ";
    double pounds;
    cin >> pounds;
  
    cout << "Number of ounces: ";
    double ounces;
    cin >> ounces;
    
    // Converting ounces to pounds
    pounds = pounds + (ounces / 16);
    
    return pounds; 
}

// 
double conversionCalc(double pound)
{
    return pound / 2.2046;
}

void conversionOutput(double kilograms)
{
    // I hope this is how you wanted the output to look like.
    // The homework questions should be more specific or give an 
    // example of how the output should look like.
    cout << endl;
    cout << "That is " << kilograms << " kilograms or " 
            << kilograms * 1000 << " grams" << endl;
}