/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: angel
 *
 * Created on September 19, 2021, 6:08 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main() {
    
    // Counter
    int counter = 0;
    
    // Sum variable
    int sum = 0;
    
    // User input
    cout << "Would you like to run the program? ";
    string input;
    cin >> input;
    
    while (tolower(input[0]) == 'y')
    {
        // Increase the counter
        counter++;
        
        cout << "Please enter a number: " << endl;
        int num;
        cin >> num;
        
        
        // Add the num from user to the total sum
        sum = sum + num;
        // sum += num  == sum = sum + num
        // var /= 5  var *= 3  we can compund operators
        
        cout << "Would you like to run again? Y/N" << endl;
        cin >> input;
    }
    
    // Error check divide by 0
    if (counter == 0)
    {
        cout << "The average is: 0" << endl;
    }
    else 
    {
        cout << "The average is: " << sum / static_cast<double>(counter) << endl;
    }
    cout << "The program ran " << counter << " times." << endl;
    
    return 0;
}


