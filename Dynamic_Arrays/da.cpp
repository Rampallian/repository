
#include <cstdlib>
#include <iostream>
using namespace std;

void output(int[], int size);
void storeRandomValues(int [], int size);
void add5(int*, int size);
int* createRandomDynamicArray(int size);

int main() 
{
    srand(time(0));
    int size = 3;
    // Static Array
    // Static - not changing address or size and located on stack
    int staticArray[3];
    
    storeRandomValues(staticArray, size);
    output(staticArray, size);
    
    // Dynamic Array
    // Use "new" keyword to allocate memory
    int* dynamicArray = createRandomDynamicArray(size);
    
    storeRandomValues(dynamicArray, size);
    output(dynamicArray, size);
    
    // Use new pointer function on both static and dynamic arrays
    add5(staticArray, size);
    add5(dynamicArray, size);
    
    output(staticArray, size);
    output(dynamicArray, size);
    
    // Deallocate dynamic array to avoid memory leak
    delete[] dynamicArray;
    
    // Static and dynamic arrays are both pointers
    // A static array variable points to the start of the array
    int a[10]; // create static array
    int* p; // create pointer
    //a = p; // assigning the pointer address to the array address -- ILLEGAL
    p = a; // assigning array address to pointer address
    
    
    
    
    return 0;
}

void output(int a[], int size)
{
    for(int i = 0; i < size; i++)
    {
        cout << a[i] << " ";
    }
    cout << endl;
}

void storeRandomValues(int a[], int size)
{
    for(int i = 0; i < size; i++)
    {
        a[i] = rand() % 11;
    }
}
/**
 * Add 5 to every value within the dynamic array
 * @param p
 * @param size
 */
void add5(int* p, int size)
{
    for(int i = 0; i < size; i++)
    {
        p[i] += 5;
    }
}
/**
 * Creates and returns a dynamic array with random values
 * @param size
 * @return 
 */
int* createRandomDynamicArray(int size)
{
    int* array = new int[size];
    
    storeRandomValues(array, size);
    
    return array;
}