#include <iostream>
#include <cstdlib>

using namespace std;

/*
 * 
 */
int main() 
{
    int value = 4;
    
    cout << value << endl;
    
    // Address of operator (&)
    
    cout << "Address of value: " << &value << endl;
    
    // Store the memory location into a pointer variable
    // Declare an integer pointer
    int* address;
    
    address = &value;
    
    cout << "Address: " << address << endl;
    
    // Retrieve the value stored at a specific address
    // Use the de-reference operator (*) to get the value
    // at an address
    cout << "Value at address: " << *address << endl;
    
    // Use the de-reference operator to modify a value
    // via its address
    *address = 10;
    
    cout << "Value: " << value << endl;
    
    // Use the new operator to allocate memory to the heap
    int* p;
    
    // The new operator returns a memory location on the heap
    // That memory is allocated for used
    p = new int;
    
    cout << "Address in heap: " << p << endl;
    cout << "Value in address: " << *p << endl;
    
    *p = 10;
    cout << "Value in address: " << *p << endl;
    
    // De-allocate memory when done with it
    // Use the delete keyword to deallocate memory
    // p the variable still exists after deallocation
    delete p;
    
    cout << p << endl;
    
    return 0;
}

