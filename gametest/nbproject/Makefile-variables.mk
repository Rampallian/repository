#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=Cygwin-Windows
CND_ARTIFACT_DIR_Debug=dist/Debug/Cygwin-Windows
CND_ARTIFACT_NAME_Debug=gametest
CND_ARTIFACT_PATH_Debug=dist/Debug/Cygwin-Windows/gametest
CND_PACKAGE_DIR_Debug=dist/Debug/Cygwin-Windows/package
CND_PACKAGE_NAME_Debug=gametest.tar
CND_PACKAGE_PATH_Debug=dist/Debug/Cygwin-Windows/package/gametest.tar
# Release configuration
CND_PLATFORM_Release=Cygwin-Windows
CND_ARTIFACT_DIR_Release=dist/Release/Cygwin-Windows
CND_ARTIFACT_NAME_Release=gametest
CND_ARTIFACT_PATH_Release=dist/Release/Cygwin-Windows/gametest
CND_PACKAGE_DIR_Release=dist/Release/Cygwin-Windows/package
CND_PACKAGE_NAME_Release=gametest.tar
CND_PACKAGE_PATH_Release=dist/Release/Cygwin-Windows/package/gametest.tar
# Debug64 configuration
CND_PLATFORM_Debug64=Cygwin-Windows
CND_ARTIFACT_DIR_Debug64=dist/Debug64/Cygwin-Windows
CND_ARTIFACT_NAME_Debug64=gametest
CND_ARTIFACT_PATH_Debug64=dist/Debug64/Cygwin-Windows/gametest
CND_PACKAGE_DIR_Debug64=dist/Debug64/Cygwin-Windows/package
CND_PACKAGE_NAME_Debug64=gametest.tar
CND_PACKAGE_PATH_Debug64=dist/Debug64/Cygwin-Windows/package/gametest.tar
# Release64 configuration
CND_PLATFORM_Release64=Cygwin-Windows
CND_ARTIFACT_DIR_Release64=dist/Release64/Cygwin-Windows
CND_ARTIFACT_NAME_Release64=gametest
CND_ARTIFACT_PATH_Release64=dist/Release64/Cygwin-Windows/gametest
CND_PACKAGE_DIR_Release64=dist/Release64/Cygwin-Windows/package
CND_PACKAGE_NAME_Release64=gametest.tar
CND_PACKAGE_PATH_Release64=dist/Release64/Cygwin-Windows/package/gametest.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
