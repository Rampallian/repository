
#include <cstdlib>
#include <iostream>
#include <string>
#include <iomanip>

using namespace std;

/*
 * 
 */


int main() 
{
    // User enters string "HELLO"
    // User sees:
    // H
    // E
    // L
    // L
    // O
    
    cout << "Please enter a string: " << endl;
    string input;
    getline(cin, input);
    
    // We have the entire string
    // We have to iterate across the entire string
    // Use the for loop and the subscript operator
    // I know the length of the string, hence the for loop
    
    // start at index 0. Loop size times
    for (int i = 0; i < input.size(); i++)
    {
        cout << input[i] << endl;
    }
    
    // Output the string in reverse
    for (int i = input.size() - 1; i >= 0; i--)
    {
        cout << input[i];
    }
    cout << endl;
    
    // Output all characters in all caps
    for (int i = 0; i < input.size(); i++)
    {
        //char c = toupper(input[i]); // Implicit conversion
        
        // Explicit conversion
        cout << static_cast<char>(toupper(input[i]));
    }
    cout << endl;
    return 0;
}

