#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;

// FUNCTIONS
void blackjack(int chips);

int cardDraw();

void craps(int& chips);

int diceroll();

void chipBetOutput(int& chips, int& chipBet);

void passBetWin(int& chips, int& passBet);
void passBetLose(int& chips, int& passBet);
void dontPassBetWin(int& chips, int& passBet);
void dontPassBetLose(int& chips, int& passBet);

void oddsBetOutput(int dice1, int& oddsBet, int& chips, int firstBet);



int main() 
{
    // MAIN MENU
    srand(time(0));
    int chips = 100;
    
    do
    {
        int gameOption;
        cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n  " << endl;
        cout << "|------------------------------------------|" << endl;
        cout << "|     Welcome to Angel's Casino Program!   |" << endl;
        cout << "| Let's hope you win a lot and let's hope  |" << endl;
        cout << "|    that I don't fail this class...       |" << endl;
        cout << "|------------------------------------------|" << endl;
        cout << "|      What game do you want to play?      |" << endl;
        cout << "|      1.) Craps                           |" << endl;
        cout << "|      2.) Blackjack                       |" << endl;
        cout << "|      3.) Exit Casino                     |" << endl;
        cout << "|------------------------------------------|" << endl;
        cout << "         Current Amount of Chips:            " << endl;
        cout << "                   " << chips << endl;
        cout << "|------------------------------------------|" << endl;
        cout << "         Input game number here:  ";
        cin >> gameOption;

        switch(gameOption)
        {
            case 1:
            {
                craps(chips);
            }
            break;
            case 2:
            {
                blackjack(chips);
            }
            break;
            case 3:
            {
                cout << "Leaving the casino..." << endl;
                exit(0);
            }
            break;
            default:
                cout << "Invalid Input! Exiting game..." << endl;
                exit(0);
        }
            
    }
    while(true);
    
    return 0;
}

void blackjack(int chips) 
{
    string replay;
    
    do
    {
    
        cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"  << endl;

        cout << "|-------------------------------|" << endl;
        cout << "|                               |" << endl;
        cout << "|     Welcome to Blackjack!     |" << endl;
        cout << "|                               |" << endl;
        cout << "|-------------------------------|" << endl;
        cout << "           Chips:  " << chips << endl;
        // Asking user how much they want to bet and subtracting their
        // input from their total chip amount
        cout << "How much do you want to bet: ";
        int chipBet;
        cin >> chipBet;

       // This is for funky edge cases...
       // If the user tries to be cheeky and doesn't put in a bet 
       // or negative number then re-prompt for another bet 
        if (chipBet <= 0)
        {
            cout << "Please input an actual bet: ";
            cin >> chipBet;
            // If they still enter an invalid number then scold them
            if (chipBet <= 0)
            {
                cout << "Don't be cheeky. Exiting..." << endl;
            }
            // Redo normal if statements after second prompt
            else if (chipBet >= chips)
            {
                chipBet = chips;
                cout << "Going All In!" << endl;
            }
            else 
            {
                chips -= chipBet;
                cout << "Betting " << chipBet << " chips" << endl;
                cout << "Current chips:  " << chips << endl;
            }   
        }
        // If the bet is bigger than or equal to the chips they have, then go ALL IN
        else if (chipBet >= chips)
        {
            chipBet = chips;
            cout << "Going All In!" << endl;
        }
        // Subtract the bet from total chips
        else
        {
            chips -= chipBet;
            cout << "Betting " << chipBet << " chips" << endl;
            cout << "Current chips:  " << chips << endl;
        }

        /* 
         * START OF BLACKJACK GAME
         * -------------------------------------------------------------------------
         */


        int dealerCard1, dealerCard2, dealerHandScore = 0;
        int userCard1, userCard2, userHandScore = 0;
        int userHandSize = 2, dealerHandSize = 2;

        string hitOrStay;

        // Made the array length 7 because there is no way
        // you can hit 5 times without going over 21
        int userHandHits[7];

        // Getting all possible draws for the user
        for (int i = 0; i < 7; i++)
        {
            userHandHits[i] = cardDraw();
        }

        int dealerHandHits[7];

        // Getting all possible draws for the dealer
        for (int i = 0; i < 7; i++)
        {
            dealerHandHits[i] = cardDraw();
        }

         // Drawing cards for players
        dealerCard1 = dealerHandHits[0];
        dealerCard2 = dealerHandHits[1];
        dealerHandScore = dealerCard1 + dealerCard2;
        userCard1 = userHandHits[0];
        userCard2 = userHandHits[1];;
        userHandScore = userCard1 + userCard2;


        cout << "Total pot:  " << chipBet * 2 << endl;
        cout << "Your hand:  " << userCard1 << "  " << userCard2 << endl;
        cout << "Total Score:  " << userHandScore << endl;
        // If the user gets 21
        if (userCard1 + userCard2 == 21)
        {
            cout << "BLACKJACK" << endl;
        }
        // If they don't get 21 then ask them to hit or stay
        else
        {
            cout << "Enter H for hit or S for stay:  ";
            cin >> hitOrStay;
            // If user hits
            if (tolower(hitOrStay[0]) == 'h')
            {
                do
                {
                    cout << "You hit" << endl;
                    // Outputting entire hand and adding 1 to hand size
                    // to give next card in array
                    cout << "Your hand:  ";
                    userHandSize++;
                    // Draw next card in preset draws(user array)
                    for (int i = 0; i < userHandSize; i++)
                    {
                        cout << userHandHits[i] << "  ";     
                    }

                    userHandScore = userHandScore + userHandHits[userHandSize - 1];

                    cout << endl;
                    cout << "Total Score:  " << userHandScore << endl;

/*
                    bool haveAces = false;
                    // Checking for aces
                    for(int i = 0; i < userHandSize; i++)
                    {
                        if(userHandHits[i] == 11)
                        {
                            haveAces = true;
                            if (haveAces == true)
                            {
                                cout << "bug" << endl;
                            }
                        }
                        cout << userHandHits[i] << "  ";
                    }
                    cout << endl;
                    if (haveAces == true && userHandScore > 21)
                    {
                        for(int i = 0; i < userHandSize; i++)
                        {
                            if(userHandHits[i] == 11)
                            {
                                userHandHits[i] = 1;
                            }
                            cout << userHandHits[i] << "  ";
                        }
                        cout << endl;
                    }
*/


                    if (userHandScore < 21)
                    {
                        cout << "[H]it or [S]tay:  ";
                        cin >> hitOrStay; 
                    }
                    // BLACKJACK
                    else if (userHandScore == 21)
                    {
                        cout << "BLACKJACK" << endl;
                    }
                    // BUST
                    else 
                    {
                        cout << "BUST" <<endl;
                        cout << "Dealer Wins!" << endl;
                    }

                }    
                while (tolower(hitOrStay[0]) == 'h' && userHandScore <= 21);
            }
            // If they stay
            else if (tolower(hitOrStay[0]) == 's')
            {
                cout << "You stayed" << endl;


            }
            else
            {
                cout << "Invalid Input!" << endl;
            }
        }
        cout << "Would you like to play again? Y/N ";
        cin >> replay;
        
    }
    while (tolower(replay[0]) == 'y');
}

int cardDraw()
{
    int card;
    
    // Random number from 1 to 13
    card = (rand() % 13) + 1;
    
    // Making Jacks(11), Queens(12), and Kings(13) into 10 for Blackjack
    if (card > 10)
    {
        return 10;
    }
    // If the draw an Ace
    else if (card == 1)
    {
        return card + 10;
    }
    else
    {
        return card;
    }
}

/**
 * This function emulates the rolling of 2 dice 
 * @return Returning the sum of both dice rolls
 */
int diceroll()
{
    int dice1 = (rand() % 6) + 1;
    int dice2 = (rand() % 6) + 1;
    
    int diceTotal = dice1 + dice2;
    
    return diceTotal;
}

/**
 * Prompts the user for their bet, then outputs their bet and their total 
 * chips afterwards. Also, it checks for weird edge cases like if the user 
 * enters in a zero or negative number.
 * @param chips This is linked to the main chips and edits it based on chipBet
 * @param chipBet the user's bet of chips. Edits the total of chips
 */
void chipBetOutput(int& chips, int& chipBet)
{
    if (chips == 0)
    {
        cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" << endl;
        cout << "YOU ARE OUT OF CHIPS. LEAVE THE CASINO" << endl;
        exit(0);
    }
    else if (chipBet <= 0)
    {
        cout << "Please input an actual bet: ";
        cin >> chipBet;
        // If they still enter an invalid number then scold them
        if (chipBet <= 0)
        {
            cout << "Don't be cheeky. Exiting..." << endl;
            exit(0);
        }
        // Redo normal if statements after second prompt
        else if (chipBet > chips)
        {
            chipBet = chips;
            cout << "You don't have that many chips!" << endl;
            cout << "SO going All In!" << endl;
        }
        else if (chipBet == chips)
        {
            chipBet = chips;
            cout << "Going All In!" << endl;
        }
        else 
        {
            chips -= chipBet;
            cout << "Betting " << chipBet << " chips" << endl;
            cout << "Current chips:  " << chips << endl;
        }   
    }
    // If the bet is bigger than the chips they have, then go ALL IN
    else if (chipBet > chips)
    {
        chipBet = chips;
        chips -= chipBet;
        cout << "You don't have that many chips!" << endl;
        cout << "SO going All In!" << endl;
    }
    // If chips equals the bet then go All In
    else if (chipBet == chips)
    {
        chipBet = chips;
        chips -= chipBet;
        cout << "Going All In!" << endl;
    }
    // Subtract the bet from total chips
    else
    {
        chips -= chipBet;
        cout << "Betting " << chipBet << " chips" << endl;
        cout << "Current chips:  " << chips << endl;
    }
}

// These functions output that they lost, or if they won and adds to their chips
// however much they won
void passBetWin(int& chips, int& passBet)
{
    cout << "You win!!!" << endl;
    chips += (passBet * 2);
    cout << "You won " << passBet * 2 << " chips" << endl;
    cout << "Chips:  " << chips << endl;
}
void passBetLose(int& chips, int& passBet)
{
    cout << "You lose!" << endl;
    cout << "Chips:  " << chips << endl;
}
void dontPassBetWin(int& chips, int& dontPassBet)
{
    cout << "You win!!!" << endl;
    chips += (dontPassBet * 2);
    cout << "You won " << dontPassBet * 2 << " chips" << endl;
    cout << "Chips:  " << chips << endl;
}
void dontPassBetLose(int& chips, int& dontPassBet)
{
    cout << "You lose!" << endl;
    cout << "Chips:  " << chips << endl;
}

void oddsBetOutput(int dice, int& oddsBet, int& chips, int firstBet)
{
    int betCap;
    
    if (dice == 4 || dice == 10)
    {
        cout << "You can only bet x3 your initial bet." << endl;
        cout << "How much do you want to bet on the Odds: ";
        cin >> oddsBet;
        betCap = firstBet * 3;
        
        // Checking if bet exceeds the multiplier cap and setting it to cap
        if (oddsBet > betCap)
        {
            cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n" << endl;
            cout << "You can't bet that much!" << endl;
            cout << "Setting bet to x3 the initial bet..." << endl;
            oddsBet = firstBet * 3;
            chips -= oddsBet;  
            cout << "Betting " << oddsBet << " chips." << endl;
            cout << "Chips:  " << chips << endl;
        }
        
        else if (oddsBet < 0)
        {
            cout << "Invalid Input!" << endl;
        }
        // If their chip count is null at this point
        else if (chips == 0)
        {
            cout << "You have no chips to bet with.." << endl;
            cout << "You better hope you win br0ke boi" << endl;
        }

        // If bet is more than chips
        else if (oddsBet >= chips)
        {
            oddsBet = chips;
            chips -= oddsBet;
            cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n" << endl;
            cout << "Betting " << oddsBet << " chips" << endl;
            cout << "Current chips:  " << chips << endl;
        }
        else
        {
            chips -= oddsBet;
            cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n" << endl;
            cout << "Betting " << oddsBet << " chips" << endl;
            cout << "Current chips:  " << chips << endl;
        }
    }
    else if (dice == 5 || dice == 9)
    {
        cout << "You can only bet x4 your initial bet." << endl;
        cout << "How much do you want to bet on the Odds: ";
        cin >> oddsBet;
        betCap = firstBet * 4;
        
        // Checking if bet exceeds the multiplier cap and setting it to cap
        if (oddsBet > betCap)
        {
            cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n" << endl;
            cout << "You can't bet that much!" << endl;
            cout << "Setting bet to x4 the initial bet..." << endl;
            oddsBet = firstBet * 4;
            chips -= oddsBet;  
            cout << "Betting " << oddsBet << " chips." << endl;
            cout << "Chips:  " << chips << endl;
        }
        
        else if (oddsBet < 0)
        {
            cout << "Invalid Input!" << endl;
        }
        // If their chip count is null at this point
        else if (chips == 0)
        {
            cout << "You have no chips to bet with.." << endl;
            cout << "You better hope you win br0ke boi" << endl;
        }

        // If bet is more than chips
        else if (oddsBet >= chips)
        {
            oddsBet = chips;
            chips -= oddsBet;
            cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n" << endl;
            cout << "Betting " << oddsBet << " chips" << endl;
            cout << "Current chips:  " << chips << endl;
        }
        else
        {
            chips -= oddsBet;
            cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n" << endl;
            cout << "Betting " << oddsBet << " chips" << endl;
            cout << "Current chips:  " << chips << endl;
        }
    }
    else if (dice == 6 || dice == 8)
    {
        cout << "You can only bet x5 your initial bet." << endl;
        cout << "How much do you want to bet on the Odds: ";
        cin >> oddsBet;
        betCap = firstBet * 5;
        
        // Checking if bet exceeds the multiplier cap and setting it to cap
        if (oddsBet > betCap)
        {
            cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n" << endl;
            cout << "You can't bet that much!" << endl;
            cout << "Setting bet to x5 the initial bet..." << endl;
            oddsBet = firstBet * 5;
            chips -= oddsBet;  
            cout << "Betting " << oddsBet << " chips." << endl;
            cout << "Chips:  " << chips << endl;
        }
        
        else if (oddsBet < 0)
        {
            cout << "Invalid Input!" << endl;
        }
        // If their chip count is null at this point
        else if (chips == 0)
        {
            cout << "You have no chips to bet with.." << endl;
            cout << "You better hope you win br0ke boi" << endl;
        }

        // If bet is more than chips
        else if (oddsBet >= chips)
        {
            oddsBet = chips;
            chips -= oddsBet;
            cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n" << endl;
            cout << "Betting " << oddsBet << " chips" << endl;
            cout << "Current chips:  " << chips << endl;
        }
        else
        {
            chips -= oddsBet;
            cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n" << endl;
            cout << "Betting " << oddsBet << " chips" << endl;
            cout << "Current chips:  " << chips << endl;
        }
        
    }
    else
    {
        cout << "If this happens then there is a bug" << endl;
    }
}

void craps(int& chips)
{
    string replay;
    
    do
    {
        srand(time(0));
        
        int dice1 = diceroll();
        
        int passBet, dontPassBet, comeBet, dontComeBet;
        int oddsBet, comePoint, dontComePoint;
        string passOrDont, dontComeOrNot, comeOrNot, oddsOrNot;
        
        
        // MAIN MENU
        cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"  << endl;
        cout << "|||||||||||||||||||||||||||||||||||" << endl;
        cout << "||       Welcome to Craps!       ||" << endl;
        cout << "||       -----------------       ||" << endl;
        cout << "|||||||||||||||||||||||||||||||||||" << endl;
        cout << "            Chips:  " << chips << endl;
        cout << "  Welcome! Start by making a bet." << endl;
        cout << "\nWould you like to [P]ass line bet or [D]on't pass bet: ";
        cin >> passOrDont;
        cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" << endl;
        
        // If they make a pass bet        
        if (tolower(passOrDont[0]) == 'p')
        {
            cout << "Chips:  " << chips << endl;
            cout << "How much do you want to bet on pass: ";
            cin >> passBet;
            chipBetOutput(chips, passBet);
        }  
        // If they make a don't pass bet
        else if (tolower(passOrDont[0]) == 'd')    
        {
            cout << "Chips:  " << chips << endl;
            cout << "How much do you want to bet on don't pass: ";
            cin >> dontPassBet;
            chipBetOutput(chips, dontPassBet);
        }
        else
        {
            cout << "Invalid Input! Exiting..." << endl;
            exit(0);
        }



        cout << "\nYou rolled a " << dice1 << endl;

        // If you roll a 2, 3, or 12
        if (dice1 == 2 || dice1 == 3 || dice1 == 12)
        {
            // Pass bets LOSE
            if (tolower(passOrDont[0]) == 'p')
            {
                passBetLose(chips, passBet);
            }
            // If don't pass bet
            else if (tolower(passOrDont[0]) == 'd')
            {
                // if dice roll is 2 or 3 then WINS
                if (dice1 == 2 || dice1 == 3)
                {
                    dontPassBetWin(chips, dontPassBet);
                }
                // if dice roll is 12 then PUSH
                else if (dice1 == 12)
                {
                    cout << "The bet is pushed" << endl;
                    cout << "No winning or losing. Returning bet..." << endl;
                    chips += dontPassBet;
                    cout << "Chips:  " << chips << endl;
                }
                else
                {
                    cout << "This really shouldn't happen" << endl;
                    exit(0);
                }
            }
            else
            {
                cout << "This shouldn't happen" << endl;
                exit(0);
            }  
        }



        // If you roll a 7 or 11
        else if(dice1 == 7 || dice1 == 11)
        {
            // Pass bets WIN
            if (tolower(passOrDont[0]) == 'p')
            {
                passBetWin(chips, passBet);
                
            }
            // don't pass bets LOSE
            else if (tolower(passOrDont[0]) == 'd')
            {
                dontPassBetLose(chips, dontPassBet);
            }
            else
            {
                cout << "This shouldn't happen" << endl;
            }
        }
        
        // ------------------------------------------------------------------------------------------
        // ---------------------------BONUS ROUND----------------------------------------------------
        // ------------------------------------------------------------------------------------------
        
        // If you roll any of the point numbers
        else 
        {
            cout << "Bonus Round!\n" << endl;
            // If they made a pass bet, ask for come or odd bets
            if (tolower(passOrDont[0]) == 'p')
            {
                // COME BET
                cout << "Would you like to make a Come bet? Y/N: "; 
                cin >> comeOrNot; 
                
                if (tolower(comeOrNot[0]) == 'y')
                {
                    cout << "How much do you want to bet on Come: ";
                    cin >> comeBet;
                    chipBetOutput(chips, comeBet);                   
                }
                else if (tolower(comeOrNot[0]) == 'n')
                {
                    //If no then don't do anything
                }
                else
                {
                    cout << "Invalid Input!" << endl;
                }
                
                // ODDS BET
                cout << "Would you like to take the Odds? Y/N: ";
                cin >> oddsOrNot;
                
                if (tolower(oddsOrNot[0]) == 'y')
                {
                    // ODDS MULTIPLYER
                    oddsBetOutput(dice1, oddsBet, chips, passBet);
                    
                    //chipBetOutput(chips, oddsBet);  
                    
                    
                }
                else if (tolower(oddsOrNot[0]) == 'n')
                {
                    //If no then don't do anything
                }
                else
                {
                    cout << "Invalid Input!" << endl;
                }
            }
            // If they made a don't pass bet, ask for don't come
            else if (tolower(passOrDont[0]) == 'd')
            {
                // DON'T COME BET
                cout << "Would you like to make a Don't Come bet? Y/N: ";
                cin >> dontComeOrNot;
                
                if (tolower(dontComeOrNot[0]) == 'y')
                {
                    cout << "How much do you want to bet on Don't Come: ";
                    cin >> dontComeBet;
                    chipBetOutput(chips, dontComeBet);                   
                }
                else if (tolower(dontComeOrNot[0]) == 'n') 
                {
                    //If no then don't do anything
                }
                else
                {
                    cout << "Invalid Input!" << endl;
                }
                
                // ODDS BET
                cout << "Would you like to take the Odds? Y/N: ";
                cin >> oddsOrNot;
                
                if (tolower(oddsOrNot[0]) == 'y')
                {
                    oddsBetOutput(dice1, oddsBet, chips, dontPassBet);              
                }
                else if (tolower(oddsOrNot[0]) == 'n')
                {
                    //If no then don't do anything
                }
                else
                {
                    cout << "Invalid Input!" << endl;
                }
            }
            else
            {
                cout << "idk if this needs to be here" << endl;
            }
            
            
            // If somebody wins or loses then stop loop
            bool winOrLose = false;
            do
            {
                // re-roll the dice
                int point = diceroll();
                cout << "You rolled a " << point << endl;

                
                // If a 7 is rolled
                if (point == 7)
                {
                    if (tolower(passOrDont[0]) == 'p')
                    {
                        // If odds were bet
                        if (tolower(oddsOrNot[0]) == 'y')
                        {
                            cout << "You lost your Pass Odds!" << endl;                          
                        }
                        // If no odds were bet then give back pass bet
                        else
                        {
                            passBetLose(chips, passBet);
                        }
                    }
                    // If user bet on don't pass
                    else //(tolower(passOrDont[0]) == 'd')
                    {
                        cout << "This executed" << endl;
                        // If odds were bet on Don't Pass
                        if (tolower(oddsOrNot[0]) == 'y')
                        {
                            int chipsWon;
                            cout << "This executed too" << endl;
                            // Outputting winnings and adding winnings to chips
                            // based off of the respective multiplier
                            if (dice1 == 4 || dice1 == 10)
                            {
                                chipsWon = (oddsBet * 1.5) + (passBet * 2);
                                cout << "You won your odds!" << endl;
                                cout << "Adding " << chipsWon << " to your chips." << endl;
                                chips += (oddsBet * 1.5) + (passBet * 2);
                                cout << "Chips:  " << chips << endl;
                            }
                            else if (dice1 == 5 || dice1 == 9)
                            {
                                chipsWon = (oddsBet * 1.67) + (passBet * 2);
                                cout << "You won your odds!!" << endl;
                                cout << "Adding " << chipsWon << " to your chips." << endl;
                                chips += (oddsBet * 1.67) + (passBet * 2);
                                cout << "Chips:  " << chips << endl;
                            }
                            else if (dice1 == 6 || dice1 == 8)
                            {
                                chipsWon = (oddsBet * 1.83) + (passBet * 2);
                                cout << "You won your odds!!!" << endl;
                                cout << "Adding " << chipsWon << " to your chips." << endl;
                                chips += (oddsBet * 1.83) + (passBet * 2);
                                cout << "Chips:  " << chips << endl;
                            }
                        }
                        // If no odds were taken then output normal don't pass win
                        else
                        {
                            cout << "You won your Don't Pass Bet!" << endl;
                            cout << "Adding " << dontPassBet * 2 << " to your chips." << endl;
                            cout << "Chips:  " << chips << endl;
                        }   
                    }
                    
                    winOrLose = true; // Exit loop
                }
                // If the same dice is rolled again
                else if (point == dice1)
                {
                    if (tolower(passOrDont[0]) == 'p')
                    {
                        // If they took odds then display odds winnings
                        if (tolower(oddsOrNot[0]) == 'y')
                        {
                            // Outputting winnings and adding winnings to chips
                            // based off of the respective multiplier
                            if (point == 4 || point == 10)
                            {
                                cout << "You won your odds!" << endl;
                                cout << "Adding " << (oddsBet * 3) + (passBet * 2) << " to your chips." << endl;
                                chips += (oddsBet * 3) + (passBet * 2);
                                cout << "Chips:  " << chips << endl;
                            }
                            else if (point == 5 || point == 9)
                            {
                                cout << "You won your odds!!" << endl;
                                cout << "Adding " << (oddsBet * 2.5) + (passBet * 2) << " to your chips." << endl;
                                chips += (oddsBet * 2.5) + (passBet * 2);
                                cout << "Chips:  " << chips << endl;
                            }
                            else if (point == 6 || point == 8)
                            {
                                cout << "You won your odds!!!" << endl;
                                cout << "Adding " << (oddsBet * 2.2) + (passBet * 2) << " to your chips." << endl;
                                chips += (oddsBet * 2.2) + (passBet * 2);
                                cout << "Chips:  " << chips << endl;
                            }
                        }
                        // If not then display normal pass line winnings
                        else
                        {
                            passBetWin(chips, passBet);
                        }
                    }
                    else
                    {
                        // If they said yes to odds
                        if (tolower(oddsOrNot[0]) == 'y')
                        {
                            cout << "You lost your Don't Pass Odds Bet!" << endl;
                        }
                        // If they didn't do odds then display normal pass win
                        else
                        {
                            cout << "You lose!" << endl;
                            cout << "Chips:  " << chips << endl;
                        }
                    }

                    winOrLose = true; // Exit loop
                }
                // If any other non-point/non-losing number is rolled
                // If the point or 7 isn't rolled, then continue rolling
                else
                {
                    winOrLose = false; // Continue loop
                }
            }
            while(winOrLose == false);
        }
        // Make fun of the user if they lost all their chips
        // and then end the program
        if (chips == 0)
        {
            cout << "You ran out of chips!" << endl;
            cout << "Come back when you ain't broke as hell" << endl;
            cout << "The house always wins" << endl;
            
            exit(0);
        }
        cout << "Would you like to play again? Y/N  ";
        cin >> replay;
    }
    while (tolower(replay[0]) == 'y');
    // END OF CRAPS ------------------------------------------------------------
    
}
